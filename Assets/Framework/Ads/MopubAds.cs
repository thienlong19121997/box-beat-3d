﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MopubAds : Ads
{
#if MOPUB_ADS

    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private string bannerAdUnitId;
    [SerializeField]
    private string interstitialAdUnitId;
    [SerializeField]
    private string rewardedVideoAdUnitId;
    #endregion

    #region Normal paramters
    private Dictionary<string, bool> adUnitToLoadedMapping;

    #endregion

    #region Encapsulate
    public string BannerAdUnitId
    {
        get
        {
            return bannerAdUnitId;
        }

        set
        {
            bannerAdUnitId = value;
        }
    }

    public string InterstitialAdUnitId
    {
        get
        {
            return interstitialAdUnitId;
        }

        set
        {
            interstitialAdUnitId = value;
        }
    }

    public string RewardedVideoAdUnitId
    {
        get
        {
            return rewardedVideoAdUnitId;
        }

        set
        {
            rewardedVideoAdUnitId = value;
        }
    }
    #endregion

    public override void Initialize()
    {
        base.Initialize();

        RegisterEvents();

        var allBannerAdUnits = new string[] { bannerAdUnitId };
        var allInterstitialAdUnits = new string[] { interstitialAdUnitId };
        var allRewardedVideoAdUnits = new string[] { rewardedVideoAdUnitId };

        adUnitToLoadedMapping = new Dictionary<string, bool>();

        AddAdUnitsToStateMaps(allBannerAdUnits);
        AddAdUnitsToStateMaps(allInterstitialAdUnits);
        AddAdUnitsToStateMaps(allRewardedVideoAdUnits);


#if UNITY_ANDROID && !UNITY_EDITOR
		MoPub.loadBannerPluginsForAdUnits (allBannerAdUnits);
		MoPub.loadInterstitialPluginsForAdUnits (allInterstitialAdUnits);
		MoPub.loadRewardedVideoPluginsForAdUnits (allRewardedVideoAdUnits);
#elif UNITY_IPHONE && !UNITY_EDITOR
		MoPub.loadPluginsForAdUnits(allBannerAdUnits);
		MoPub.loadPluginsForAdUnits(allInterstitialAdUnits);
		MoPub.loadPluginsForAdUnits(allRewardedVideoAdUnits);
#endif

#if !UNITY_EDITOR
		if (!IsAdUnitArrayNullOrEmpty (allRewardedVideoAdUnits)) {
			MoPub.initializeRewardedVideo ();
		}
#endif

#if !(UNITY_ANDROID || UNITY_IPHONE)
		Debug.LogWarning("Please switch to either Android or iOS platforms to run sample app!");
#endif
                
        MoPub.requestInterstitialAd(interstitialAdUnitId);
        MoPub.requestRewardedVideo(rewardedVideoAdUnitId);
    }

    public override void Release()
    {
        base.Release();

        UnregisterEvents();
    }

    public override bool IsInterstitialAdReady()
    {
        return adUnitToLoadedMapping[interstitialAdUnitId];
    }

    public override void ShowInterstitialAd()
    {
        base.ShowInterstitialAd();

        if (IsInterstitialAdReady())
        {
            MoPub.showInterstitialAd(interstitialAdUnitId);
        }
        else
        {
            MoPub.requestInterstitialAd(interstitialAdUnitId);
        }
    }

    public override bool IsRewardVideoAdReady()
    {
        return adUnitToLoadedMapping[rewardedVideoAdUnitId];
    }

    public override void ShowRewardVideoAd()
    {
        base.ShowRewardVideoAd();

        if (IsRewardVideoAdReady())
        {
            MoPub.showRewardedVideo(rewardedVideoAdUnitId);
        }
        else
        {
            MoPub.requestRewardedVideo(rewardedVideoAdUnitId);
        }
    }

    private bool IsAdUnitArrayNullOrEmpty(string[] adUnitArray)
    {
        return (adUnitArray == null || adUnitArray.Length == 0);
    }

    private void AddAdUnitsToStateMaps(string[] adUnits)
    {
        foreach (string adUnit in adUnits)
        {
            adUnitToLoadedMapping.Add(adUnit, false);
            // Only banners need this map, but init for all to keep it simple
            //_bannerAdUnitToShownMapping.Add(adUnit, false);
        }
    }

    private void RegisterEvents()
    {
        // Listen to all events for illustration purposes
        MoPubManager.onAdLoadedEvent += onAdLoadedEvent;
        MoPubManager.onAdFailedEvent += onAdFailedEvent;
        MoPubManager.onAdClickedEvent += onAdClickedEvent;
        MoPubManager.onAdExpandedEvent += onAdExpandedEvent;
        MoPubManager.onAdCollapsedEvent += onAdCollapsedEvent;

        MoPubManager.onInterstitialLoadedEvent += onInterstitialLoadedEvent;
        MoPubManager.onInterstitialFailedEvent += onInterstitialFailedEvent;
        MoPubManager.onInterstitialShownEvent += onInterstitialShownEvent;
        MoPubManager.onInterstitialClickedEvent += onInterstitialClickedEvent;
        MoPubManager.onInterstitialDismissedEvent += onInterstitialDismissedEvent;
        MoPubManager.onInterstitialExpiredEvent += onInterstitialExpiredEvent;

        MoPubManager.onRewardedVideoLoadedEvent += onRewardedVideoLoadedEvent;
        MoPubManager.onRewardedVideoFailedEvent += onRewardedVideoFailedEvent;
        MoPubManager.onRewardedVideoExpiredEvent += onRewardedVideoExpiredEvent;
        MoPubManager.onRewardedVideoShownEvent += onRewardedVideoShownEvent;
        MoPubManager.onRewardedVideoClickedEvent += onRewardedVideoClickedEvent;
        MoPubManager.onRewardedVideoFailedToPlayEvent += onRewardedVideoFailedToPlayEvent;
        MoPubManager.onRewardedVideoReceivedRewardEvent += onRewardedVideoReceivedRewardEvent;
        MoPubManager.onRewardedVideoClosedEvent += onRewardedVideoClosedEvent;
        MoPubManager.onRewardedVideoLeavingApplicationEvent += onRewardedVideoLeavingApplicationEvent;
    }

    private void UnregisterEvents()
    {
        // Remove all event handlers
        MoPubManager.onAdLoadedEvent -= onAdLoadedEvent;
        MoPubManager.onAdFailedEvent -= onAdFailedEvent;
        MoPubManager.onAdClickedEvent -= onAdClickedEvent;
        MoPubManager.onAdExpandedEvent -= onAdExpandedEvent;
        MoPubManager.onAdCollapsedEvent -= onAdCollapsedEvent;

        MoPubManager.onInterstitialLoadedEvent -= onInterstitialLoadedEvent;
        MoPubManager.onInterstitialFailedEvent -= onInterstitialFailedEvent;
        MoPubManager.onInterstitialShownEvent -= onInterstitialShownEvent;
        MoPubManager.onInterstitialClickedEvent -= onInterstitialClickedEvent;
        MoPubManager.onInterstitialDismissedEvent -= onInterstitialDismissedEvent;
        MoPubManager.onInterstitialExpiredEvent -= onInterstitialExpiredEvent;

        MoPubManager.onRewardedVideoLoadedEvent -= onRewardedVideoLoadedEvent;
        MoPubManager.onRewardedVideoFailedEvent -= onRewardedVideoFailedEvent;
        MoPubManager.onRewardedVideoExpiredEvent -= onRewardedVideoExpiredEvent;
        MoPubManager.onRewardedVideoShownEvent -= onRewardedVideoShownEvent;
        MoPubManager.onRewardedVideoFailedToPlayEvent -= onRewardedVideoFailedToPlayEvent;
        MoPubManager.onRewardedVideoReceivedRewardEvent -= onRewardedVideoReceivedRewardEvent;
        MoPubManager.onRewardedVideoClosedEvent -= onRewardedVideoClosedEvent;
        MoPubManager.onRewardedVideoLeavingApplicationEvent -= onRewardedVideoLeavingApplicationEvent;
    }

    #region Banner Events

    private void onAdLoadedEvent(float height)
    {
        Debug.Log("onAdLoadedEvent. height: " + height);
    }

    private void onAdFailedEvent(string errorMsg)
    {
        Debug.Log("onAdFailedEvent: " + errorMsg);
    }

    private void onAdClickedEvent(string adUnitId)
    {
        Debug.Log("onAdClickedEvent: " + adUnitId);
    }

    private void onAdExpandedEvent(string adUnitId)
    {
        Debug.Log("onAdExpandedEvent: " + adUnitId);
    }

    private void onAdCollapsedEvent(string adUnitId)
    {
        Debug.Log("onAdCollapsedEvent: " + adUnitId);
    } 
    
    #endregion

    #region Interstitial Events

    private void onInterstitialLoadedEvent(string adUnitId)
    {
        Debug.Log("onInterstitialLoadedEvent: " + adUnitId);
        adUnitToLoadedMapping[adUnitId] = true;
    }

    private void onInterstitialFailedEvent(string errorMsg)
    {
        Debug.Log("onInterstitialFailedEvent: " + errorMsg);
    }

    private void onInterstitialShownEvent(string adUnitId)
    {
        Debug.Log("onInterstitialShownEvent: " + adUnitId);
    }

    private void onInterstitialClickedEvent(string adUnitId)
    {
        Debug.Log("onInterstitialClickedEvent: " + adUnitId);
    }

    private void onInterstitialDismissedEvent(string adUnitId)
    {
        Debug.Log("onInterstitialDismissedEvent: " + adUnitId);
        adUnitToLoadedMapping[adUnitId] = false;

        MoPub.requestInterstitialAd(adUnitId);
    }

    private void onInterstitialExpiredEvent(string adUnitId)
    {
        Debug.Log("onInterstitialExpiredEvent: " + adUnitId);
    } 

    #endregion
    
    #region Rewarded Video Events

    private void onRewardedVideoLoadedEvent(string adUnitId)
    {
        Debug.Log("onRewardedVideoLoadedEvent: " + adUnitId);
        adUnitToLoadedMapping[adUnitId] = true;
    }

    private void onRewardedVideoFailedEvent(string errorMsg)
    {
        Debug.Log("onRewardedVideoFailedEvent: " + errorMsg);
    }

    private void onRewardedVideoExpiredEvent(string adUnitId)
    {
        Debug.Log("onRewardedVideoExpiredEvent: " + adUnitId);
    }

    private void onRewardedVideoShownEvent(string adUnitId)
    {
        Debug.Log("onRewardedVideoShownEvent: " + adUnitId);
    }

    private void onRewardedVideoClickedEvent(string adUnitId)
    {
        Debug.Log("onRewardedVideoClickedEvent: " + adUnitId);
    }

    private void onRewardedVideoFailedToPlayEvent(string errorMsg)
    {
        Debug.Log("onRewardedVideoFailedToPlayEvent: " + errorMsg);
    }

    private void onRewardedVideoReceivedRewardEvent(MoPubManager.RewardedVideoData rewardedVideoData)
    {
        Debug.Log("onRewardedVideoReceivedRewardEvent: " + rewardedVideoData);
    }

    private void onRewardedVideoClosedEvent(string adUnitId)
    {
        Debug.Log("onRewardedVideoClosedEvent: " + adUnitId);
        adUnitToLoadedMapping[adUnitId] = false;

        MoPub.requestRewardedVideo(adUnitId);
    }

    private void onRewardedVideoLeavingApplicationEvent(string adUnitId)
    {
        Debug.Log("onRewardedVideoLeavingApplicationEvent: " + adUnitId);
    }

    #endregion

#endif
}
