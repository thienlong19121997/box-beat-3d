﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticalPooler : MonoBehaviour {
	public static ParticalPooler instance;

	public GameObject perfectPrefab;
	public List<GameObject> perfectList = new List<GameObject> ();

	public GameObject goodPrefab;
	public List<GameObject> goodList = new List<GameObject> ();

	void Awake () {
		instance = this;
		Generate ();
	}

	void Generate(){
		for (int i = 0; i < 30; i++) {
			GameObject objectClone = Instantiate (perfectPrefab) as GameObject;
			objectClone.transform.SetParent (transform);
			objectClone.SetActive (false);
			perfectList.Add (objectClone);

			GameObject goodClone = Instantiate (goodPrefab) as GameObject;
			goodClone.transform.SetParent (transform);
			goodClone.SetActive (false);
			goodList.Add (objectClone);
		}
	}


	public GameObject GetObjectPerfect(){
		for (int i = 0; i < perfectList.Count; i++) {
			if (perfectList [i].activeInHierarchy == false) {
				perfectList [i].SetActive (true);
				return perfectList [i];
			}
		}

		GameObject objectClone = Instantiate (perfectPrefab) as GameObject;
		objectClone.transform.SetParent (transform);
		objectClone.SetActive (true);
		perfectList.Add (objectClone);
		return objectClone;
	}


	public GameObject GetObjectGood(){
		for (int i = 0; i < goodList.Count; i++) {
			if (goodList [i].activeInHierarchy == false) {
				goodList [i].SetActive (true);
				return goodList [i];
			}
		}

		GameObject objectClone = Instantiate (goodPrefab) as GameObject;
		objectClone.transform.SetParent (transform);
		objectClone.SetActive (true);
		goodList.Add (objectClone);
		return objectClone;
	}

	public void Refresh(){
		for (int i = 0; i < goodList.Count; i++) {
			goodList [i].SetActive (false);
		}
		for (int i = 0; i < perfectList.Count; i++) {
			perfectList [i].SetActive (false);
		}
	}
}
