﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClassicPanel : MonoBehaviour {

	public static ClassicPanel instance;


	public class StarScript{
		public Image[] stars_fill;
		public Image[] kings_fill;
	}


	public GameObject parent;
	private int sumChild;
	private GameObject[] songs;


	private GameObject[] star;
	private GameObject[] king;

	private StarScript[] starScript;

	public GameObject[] parentLOCK;
	private Text[] stt_songs_1;
	private List<Text> stt_songs_2 = new List<Text>();
	private Text[] stt_name_songs_1;
	private List<Text> stt_name_songs_2 = new List<Text>();
	public Text[] levelText;

	//
	private GameObject[] btnPlay;
	private GameObject[] btnBuy;


	public string[] nameSongs_Classic;
	public int maxLevel;


	public GameObject[] btnStopTrySongs;
	public GameObject[] btnTrySongs;

	void Awake(){
		instance = this;
		OnStart ();
	}

	void OnStart(){
		sumChild    = parent.transform.childCount;
		songs       = new GameObject[sumChild];
		star        = new GameObject[sumChild];
		king        = new GameObject[sumChild];
		starScript  = new StarScript[sumChild];
		stt_songs_1 = new Text[sumChild];
		stt_name_songs_1 = new Text[sumChild];
		btnPlay     = new GameObject[sumChild];	
		btnBuy      = new GameObject[sumChild];
		levelText   = new Text[maxLevel];
		parentLOCK = new GameObject[maxLevel];
		btnStopTrySongs = new GameObject[sumChild];
		btnTrySongs = new GameObject[sumChild];

		for (int i = 0; i < sumChild; i++) {
			songs [i] = parent.transform.GetChild (i).gameObject;
			star [i] = songs [i].transform.GetChild(4).GetChild (1).gameObject;
			king [i] = songs [i].transform.GetChild(4).GetChild (2).gameObject;
			StarScript ss = new StarScript();
			starScript [i] = ss;
			starScript[i].stars_fill = new Image[3];
			starScript[i].kings_fill = new Image[3];
			for (int j = 0; j < 3; j++) {
				starScript [i].stars_fill [j] = star [i].transform.GetChild (j).transform.GetChild (1).GetComponent<Image> ();
				starScript [i].kings_fill [j] = king [i].transform.GetChild (j).transform.GetChild (1).GetComponent<Image> ();
			}
			btnPlay [i] = songs [i].transform.GetChild (4).gameObject;
			btnBuy  [i] = songs [i].transform.GetChild (5).gameObject;
			btnStopTrySongs [i] = songs [i].transform.GetChild (3).GetChild (2).gameObject;
			btnTrySongs [i] = songs [i].transform.GetChild (3).GetChild (0).gameObject;
			int a = i;
			btnStopTrySongs [a].GetComponent<Button> ().onClick.AddListener (delegate() {
				BtnStopTrySong();
			});
			btnTrySongs [a].GetComponent<Button> ().onClick.AddListener (delegate() {
				BtnTrySong(a);
			});
		}

		for (int i = 0; i < parentLOCK.Length; i++) {
			if (i == 0 ) {
				parentLOCK [i] = songs [4].transform.GetChild (10).gameObject;
			} else {
				parentLOCK [i] = songs [(i+1) * 4].transform.GetChild (10).gameObject;
			}
		}

		for (int i = 0; i < parentLOCK.Length; i++) {
			if (i == 0) {
				levelText [i] = songs [1].transform.GetChild (8).GetComponent<Text> ();
			} else {
				levelText [i] = songs [i*4+1].transform.GetChild (8).GetComponent<Text> ();
			}
			levelText [i].text ="Level " + (i + 1).ToString ();
		}

		UpdateMenuHome ();
		INIT_ListSong ();
	}

	void OnEnable(){
		UpdateSong_Classic ();
		UpdateMenuHome ();
	}

	public void UpdateMenuHome(){
		int c = PlayerPrefs.GetInt ("level")*4;
		for (int i = 0; i <= c; i++) {
			if (PlayerPrefs.GetInt ("mode_king_song" + i+ "classic") >= 1) {
				star [i].SetActive (false);
				king [i].SetActive (true);
			} else {
				king [i].SetActive (false);
				star [i].SetActive (true);
			}
			for (int j = 0; j < 3; j++) {
				starScript [i].stars_fill [j].fillAmount = PlayerPrefs.GetFloat ("starfill_"+j+"_song" + i + "classic");
				starScript [i].kings_fill [j].fillAmount = PlayerPrefs.GetFloat ("kingfill_"+j+"_song" + i + "classic");
			}
		}
	}

	public void INIT_ListSong(){
		for (int i = 0; i < sumChild; i++) {
			stt_songs_1 [i] = songs [i].transform.GetChild (7).gameObject.GetComponent<Text> ();
			stt_name_songs_1 [i] = songs [i].transform.GetChild (6).gameObject.GetComponent<Text> ();


		}
		for (int i = 0; i < parentLOCK.Length; i++) {
			for (int j = 0; j < 4; j++) {
				stt_songs_2.Add (parentLOCK [i].transform.GetChild(j).GetChild(2).GetComponent<Text>());
				stt_name_songs_2.Add (parentLOCK [i].transform.GetChild (j).GetChild (1).GetComponent<Text> ());
			}
		}
//
		for (int i = 0; i < sumChild; i++) {
			stt_songs_1 [i].text = i.ToString ();
			stt_name_songs_1 [i].text = nameSongs_Classic [i];

			if (i >= 1) {
				stt_songs_2 [i - 1].text = i.ToString ();
				stt_name_songs_2 [i - 1].text =  nameSongs_Classic [i];
			}
			
		}


	}

	public void UpdateSong_Classic(){
		int l = (PlayerPrefs.GetInt ("level")+1);
		for (int i = 1; i <= l; i++) {
			if (PlayerPrefs.GetInt ("level") >= i) {
				parentLOCK [i-1].SetActive (false);
			} else {
				parentLOCK [i-1].SetActive (true);
			}
		}
		int c = PlayerPrefs.GetInt ("level")*4;
		for (int i = 1; i <= c; i++) {
			if (PlayerPrefs.GetInt ("active_classic_song" + i) == 1) {
				btnPlay [i].SetActive (true);
				btnBuy [i].SetActive (false);
			} else{
				btnPlay [i].SetActive (false);
				btnBuy [i].SetActive (true);
			}
		}

	}

	public void Buy_Song_Classic(int num){
		if (PlayerPrefs.GetFloat ("gem") < 200) {
			UIManager.instance.BtnStore ();

			return;
		}

		Manager.instance.MinusGem (200);
		PlayerPrefs.SetInt ("active_classic_song" + num, 1);
		UpdateSong_Classic ();
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.livepop);
	}

	void Update(){
//		Debug.Log (songs [8].transform.position.y); // 0 y = 20  |  4 y = 25 -130
		for (int i = 0; i < maxLevel; i++) {
			if (i == 0) {
				if (songs [0].transform.position.y > 40 || songs [0].transform.position.y < -200) {
					songs [0].SetActive (false);
				} else {
					songs [0].SetActive (true);
				}
			} 
			if (songs [(i + 1) * 4].transform.position.y > 40 || songs [(i + 1) * 4].transform.position.y < -200) {
				for (int j = 0; j < 4; j++) {
					songs [(i + 1) * 4-j].SetActive (false);
				}
			} else {
				for (int j = 0; j < 4; j++) {
					songs [(i + 1) * 4-j].SetActive (true);
				}		
			}
				

		}
	}

	public void BtnTrySong(int num){
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.button);

		BtnStopTrySong ();
		BackgroundMusic.instance.OnClickTrySong_Classic (num);
		btnStopTrySongs [num].SetActive (true);
	}

	public void BtnStopTrySong(){
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.button);

		BackgroundMusic.instance.PlayMusicLastSong ();

		BackgroundMusic.instance.OnClickStopTrySong ();
		for (int i = 0; i < sumChild; i++) {
			if (btnStopTrySongs [i].activeInHierarchy) {
				btnStopTrySongs [i].SetActive (false);
			}
		}
	}
}
