﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Note : MonoBehaviour {


	// Use this for initialization
	private Renderer rend;
	private Rigidbody rig;
	private float speed = 2f;  //2 -> 2.1
	private float rotateSpeed;
	public float startSample;
	public float currenSample;
	public float timeEvent;
	public float timeLerp;
	public float currentTime;
	private float currentPosY;
	private float currentPosX;
	private int myLane;
	bool isForce;
	bool isCol;
	public bool isFall;
	public Material[] materials;
	private Material mat;

	float timefixed = 2.1f;
	bool istime;
	float time;
	public float testTIme;
	public Vector3 target;
	public bool canAction;

	void Awake(){
		rig = GetComponent<Rigidbody> ();
		rend = GetComponent<Renderer> ();
		mat = GetComponent<Renderer> ().material;
	}

	public void OnNote(int lane){
		transform.position = new Vector3 (-500, -500, -500);
		canAction = true;
		speed = 2;
		isFall = false;
		gameObject.GetComponent<MeshRenderer> ().material = materials [Random.Range(0,materials.Length)];
		rotateSpeed = Random.Range (10, 50) ;
//		lane = Random.Range (0, 6);
		myLane = lane;
//			Debug.Log ("MYLANE" + myLane);
		
		target = Manager.instance.lanes [myLane].transform.position;
		currentPosY = Random.Range (-50, 50) / 10;
		currentPosX = Random.Range (-50, 50) / 10;

		timeEvent = 4;
		timeLerp = timeEvent - timefixed ;

		gameObject.SetActive (true);
	}
		
	void DelayActive(){
		gameObject.SetActive (true);

	}

	void Update(){
		if (canAction) {
			transform.Rotate (rotateSpeed * Time.deltaTime, rotateSpeed * Time.deltaTime, rotateSpeed * Time.deltaTime);
			if (isFall == false) {
				float deltaT = (Koreo.instance.sample - currenSample) / (startSample - currenSample - 44100 * timefixed);
				testTIme = deltaT;
				if (deltaT <= 1) {
					float T = deltaT;
					float z = Mathf.Lerp (10, 0, T);
					float y = Mathf.Lerp (currentPosY, 7, T);
					float x = Mathf.Lerp (currentPosX, target.x, T);
					float c = Mathf.Lerp (0, .35f, T);

					transform.position = new Vector3 (x, y, z);
					rend.material.color = new Color (rend.material.color.r, rend.material.color.g, rend.material.color.b, c);

				} else {
					isFall = true;
					transform.position = new Vector3 (target.x, 7, 0);
					istime = true;
				}
			}
//			if (istime) {
//				time += Time.deltaTime;
//			}
//			if (transform.position.y <= -2.5f) {
//				istime = false;
////			Debug.Log (time);
//			}
		}
	}

	// Update is called once per frame
	void FixedUpdate () {
		if (canAction) {
			if (isForce == false && isFall) {
				rig.velocity = new Vector3 (0, -speed, 0);
				if (rend.material.color.a < 1) {
					float c = Mathf.Lerp (.35f, 1, 40 * Time.deltaTime);
					rend.material.color = new Color (rend.material.color.r, rend.material.color.g, rend.material.color.b, c);
				}
			}
		}
	}



	void OnTriggerEnter(Collider col){
		if (col.tag == "Line") {
			speed *= 5f;
		} else if (col.tag == "Miss") {
			StartCoroutine (RecycleMiss ());
			Manager.instance.Rate ("Miss");

		}
//		
		if (isCol == false) {
			if (col.tag == "Perfect") {
				EffectHit ("Perfect");
				Manager.instance.Rate ("Perfect");
				StartCoroutine (RecycleHit ());

				isCol = true;
			} else if (col.tag == "Good") {
				EffectHit ("Good");
				Manager.instance.Rate ("Good");
				StartCoroutine (RecycleHit ());

				isCol = true;
			}
		}
	}

	IEnumerator RecycleMiss(){
		yield return new WaitForSeconds (1);
		canAction = false;
		rig.velocity = Vector3.zero;
		rig.useGravity = false;
		rig.mass = 1;
		isForce = false;
		isFall = false;
//		transform.localPosition = new Vector3 (0, 7, 0);
		isCol = false;
		startSample = currenSample = 0;
		rend.material.color = new Color (rend.material.color.r, rend.material.color.g, rend.material.color.b, 0);
		gameObject.SetActive (false);
	}

	IEnumerator RecycleHit(){

		rig.velocity = Vector3.zero;
		rig.useGravity = true;
		rig.mass = 1.5f;
		isForce = true;
		float r = Random.Range (0, 2);
		float forceX;
		if (r == 0) {
			forceX = Random.Range (400, 600);
		} else {
			forceX = Random.Range (-400, -600);
		}
		float forceY = Random.Range(500,750) ;
		rig.AddForce (new Vector3 (forceX,forceY, 0));
		yield return new WaitForSeconds (2);
		canAction = false;

		rig.velocity = Vector3.zero;
		rig.useGravity = false;
		rig.mass = 1;
		isForce = false;
		isFall = false;
//		transform.localPosition = new Vector3 (0, 7, 0);
		isCol = false;
		startSample = currenSample = timeEvent = timeLerp = 0;
		rend.material.color = new Color (rend.material.color.r, rend.material.color.g, rend.material.color.b, 0);

		gameObject.SetActive (false);
	}

	GameObject particleClone;
	public void EffectHit(string type){
		if (type == "Perfect") {
			Debug.Log (" effect perfect");
			particleClone = ParticalPooler.instance.GetObjectPerfect ();

		} else if (type == "Good") {
			Debug.Log (" effect Good");
			particleClone = ParticalPooler.instance.GetObjectGood ();

		} 
		particleClone.transform.position = Manager.instance.frame.transform.position ;
		var main = particleClone.GetComponent<ParticleSystem> ().main;
		var mainchild = particleClone.transform.GetChild (0).GetComponent<ParticleSystem> ().main;
		main.startColor = new ParticleSystem.MinMaxGradient (rend.material.color);
		mainchild.startColor = new ParticleSystem.MinMaxGradient (rend.material.color);
		

	}

	public void Refresh(){
		if (rig == null) {
			rig = GetComponent<Rigidbody> ();
			rend = 		rend = GetComponent<Renderer> ();

		}
		rig.velocity = Vector3.zero;
		rig.useGravity = false;
		rig.mass = 1;
		isForce = false;
		isFall = false;
		//		transform.localPosition = new Vector3 (0, 7, 0);
		isCol = false;
		startSample = currenSample = timeEvent = timeLerp = 0;
		rend.material.color = new Color (rend.material.color.r, rend.material.color.g, rend.material.color.b, 0);
		canAction = false;
		gameObject.SetActive (false);
	}
}
