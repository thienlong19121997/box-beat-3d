﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOver : MonoBehaviour {

	public GameObject stars;
	public Image[] fill_stars;
	public Image[] fill_stars_target;
	public Animator[] anim_stars;
	public Animator[] shadow_anim_stars;
	public GameObject[] highlight_stars;
	bool isStarAnim0,isStarAnim1,isStarAnim2;

	public GameObject kings;
	public Image[] fill_kings;
	public Image[] fill_kings_target;
	public Animator[] anim_kings;
	public Animator[] shadow_anim_kings;
	public GameObject[] highlight_kings;
	bool isKingAnim0,isKingAnim1,isKingAnim2;

	public GameObject btn;
	bool isModeKing;
	float time = 1;
	bool isPlusLife;
	bool isShowBtn;

	// level up
	public GameObject levelup;
	public Image target_image;
	public Sprite[] target_sprites;
	public Text level_text;
	public Text level_text2;
	public int[] target_exp;

	//score
	public Text scoreText;
	public Text bestscoreText;
	public Text nameSong;

	void OnEnable(){
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.lose);

		nameSong.text = Koreo.instance.namesong;
		levelup.SetActive (false);
		isModeKing = false;
		isShowBtn = false;
		isPlusLife = false;
		time = 1;
		btn.SetActive (false);
		isStarAnim0 = false;
		isStarAnim1 = false;
		isStarAnim2 = false;
		isKingAnim0 = false;
		isKingAnim1 = false;
		isKingAnim2 = false;

		for (int i = 0; i < 3; i++) {
			highlight_kings [i].SetActive (false);
			highlight_stars [i].SetActive (false);
		}

		fill_stars_target = Manager.instance.stars;
		fill_kings_target = Manager.instance.kings;

		for (int i = 0; i < fill_stars.Length; i++) {
			fill_stars [i].fillAmount = 0;
			fill_kings [i].fillAmount = 0;
			anim_stars [i].gameObject.transform.localScale = Vector3.one * .7f;
			anim_kings [i].gameObject.transform.localScale = Vector3.one * .7f;
			highlight_stars [i].SetActive (false);
			highlight_kings [i].SetActive (false);
		}

		stars.SetActive (true);
		kings.SetActive (false);

		scoreText.text = Manager.instance.score.ToString();
		bestscoreText.text = "Best " + Manager.instance.BestScore ().ToString();
	}

	void Update(){
		if (time > 0) {
			time -= Time.deltaTime;
			return;
		}


		if (isModeKing == false) {
			UpdateStars ();
		} else {
			UpdateKings ();
		}
	}

	void UpdateStars(){
		if (fill_stars [0].fillAmount < fill_stars_target [0].fillAmount) {
			fill_stars[0].fillAmount += Time.deltaTime*1.4f;
		} else {
			if (isStarAnim0 == false) {
				isStarAnim0 = true;
				if (fill_stars [0].fillAmount >= 1) {
					EffectMusic.instance.PlayOneShot (EffectMusic.instance.musical);

					anim_stars [0].SetTrigger ("highlight");
					shadow_anim_stars [0].SetTrigger ("highlight");
					if (PlayerPrefs.GetInt ("plus_exp_star_0_song" + Koreo.instance.numSong + Koreo.instance.mode) == 0) {
						PlusExp ();
						PlayerPrefs.SetInt ("plus_exp_star_0_song" + Koreo.instance.numSong + Koreo.instance.mode, 1);
					}
//					highlight_stars [0].SetActive (true);
					RewardManager.instance.Set_Collect_3_Star (1);
				}
				Debug.Log ("complete star 0");
			}
			if (fill_stars [1].fillAmount < fill_stars_target [1].fillAmount) {
				fill_stars [1].fillAmount += Time.deltaTime*1.4f;
			} else {
				if (isStarAnim1 == false) {
					isStarAnim1 = true;
					if (fill_stars [1].fillAmount >= 1) {
						EffectMusic.instance.PlayOneShot (EffectMusic.instance.musical);

						anim_stars [1].SetTrigger ("highlight");
						shadow_anim_stars [1].SetTrigger ("highlight");
						if (PlayerPrefs.GetInt ("plus_exp_star_1_song" + Koreo.instance.numSong + Koreo.instance.mode) == 0) {
							PlusExp ();
							PlayerPrefs.SetInt ("plus_exp_star_1_song" + Koreo.instance.numSong + Koreo.instance.mode, 1);
						}

//						highlight_stars [1].SetActive (true);
						RewardManager.instance.Set_Collect_3_Star (1);
						isPlusLife = true;
						Manager.instance.PlusLive (1);
					}
					Debug.Log ("complete star 1");
				}
				if (fill_stars [2].fillAmount < fill_stars_target [2].fillAmount) {
					fill_stars [2].fillAmount += Time.deltaTime*1.4f;
				} else {
					if (isStarAnim2 == false) {
						isStarAnim2 = true;
						if (fill_stars [2].fillAmount >= 1) {
							EffectMusic.instance.PlayOneShot (EffectMusic.instance.musical);

							anim_stars [2].SetTrigger ("highlight");
							shadow_anim_stars [2].SetTrigger ("highlight");
							if (PlayerPrefs.GetInt ("plus_exp_star_2_song" + Koreo.instance.numSong + Koreo.instance.mode) == 0) {
								PlusExp ();
								PlayerPrefs.SetInt ("plus_exp_star_2_song" + Koreo.instance.numSong + Koreo.instance.mode, 1);
							}

//							highlight_stars [2].SetActive (true);
							RewardManager.instance.Set_Collect_3_Star (1);


							if (Koreo.instance.isModeKing == false) {
//								isShowBtn = true;
//								btn.SetActive (true);
								if (isShowBtn == false) {
									isShowBtn = true;
									Invoke ("ActiveBtn", 1.1f);
									LevelUp ();
								}
								return;
							}
							Invoke ("ChangeUpdate", 1);

						} else {

							if (isShowBtn == false) {
								isShowBtn = true;
								Invoke ("ActiveBtn", 1.1f);
								LevelUp ();
							}
						}
//						btn.SetActive (true);
						Debug.Log ("complete star 2");

					}
				}
			}
		}
	}

	void ChangeUpdate(){
		time = 1;
		isModeKing = true;
		stars.SetActive (false);
		kings.SetActive (true);
	}

	void UpdateKings(){
		if (fill_kings [0].fillAmount < fill_kings_target [0].fillAmount) {
			fill_kings[0].fillAmount += Time.deltaTime*1.4f;
		} else {
			if (isKingAnim0 == false) {
				isKingAnim0 = true;
				if (fill_kings [0].fillAmount >= 1) {
					EffectMusic.instance.PlayOneShot (EffectMusic.instance.musical);

					anim_kings [0].SetTrigger ("highlight");
					shadow_anim_kings [0].SetTrigger ("highlight");
//					highlight_kings [0].SetActive (true);
				}
				Debug.Log ("complete star 0");
			}
			if (fill_kings [1].fillAmount < fill_kings_target [1].fillAmount) {
				fill_kings [1].fillAmount += Time.deltaTime*1.4f;
			} else {
				if (isKingAnim1 == false) {
					isKingAnim1 = true;
					if (fill_kings [1].fillAmount >= 1) {
						EffectMusic.instance.PlayOneShot (EffectMusic.instance.musical);

						anim_kings [1].SetTrigger ("highlight");
						shadow_anim_kings [1].SetTrigger ("highlight");
//						highlight_kings [0].SetActive (true);
					}
					Debug.Log ("complete star 1");
				}
				if (fill_kings [2].fillAmount < fill_kings_target [2].fillAmount) {
					fill_kings [2].fillAmount += Time.deltaTime*1.4f;
				} else {
					if (isKingAnim2 == false) {
						isKingAnim2 = true;
						if (fill_kings [2].fillAmount >= 1) {
							EffectMusic.instance.PlayOneShot (EffectMusic.instance.musical);

							anim_kings [2].SetTrigger ("highlight");
							shadow_anim_kings [2].SetTrigger ("highlight");
//							highlight_kings [0].SetActive (true);
						} else {
							if (isShowBtn == false) {
								isShowBtn = true;
								LevelUp ();
								Invoke ("ActiveBtn", 1.1f);
							}
						}
//						btn.SetActive (true);
//						isShowBtn = true;

						Debug.Log ("complete star 2");
					}
				}
			}
		}
	}

	void ActiveBtn(){

		btn.SetActive (true);

	}

	void ShowADS(){
		UnityAdsManager.Instance.ShowAds ();
	}

	public void UpdateLevelText2(){
		level_text2.text = GetLevel ().ToString();
	}

	public void LevelUp(){
		if (GetExp () < GetTargetExp ()) {
			Invoke ("ShowADS", .8f);
			Debug.Log (GetExp () + " __ " + GetTargetExp ());
			return;
		}

		PlusLevel ();
		level_text.text = GetLevel ().ToString();
		level_text2.text = GetLevel ().ToString();

//		target_image.sprite = target_sprites [GetLevel ()];
		Invoke ("ActiveLeveUp", .4f);

		Debug.Log (GetLevel ());
//		Invoke ("ShowADS", 1.5f);
		Invoke("DelaySoundLevelUp",1.1f);

	}

	void DelaySoundLevelUp(){
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.levelup);

	}

	void ActiveLeveUp(){
		levelup.SetActive (true);

	}

	void PlusLevel(){
		int l = GetLevel ();
		l++;
		PlayerPrefs.SetInt ("level", l);
	}

	int GetLevel(){
		return PlayerPrefs.GetInt ("level");
	}

	void PlusExp(){
		int e = GetExp ();
		e++;
		PlayerPrefs.SetInt ("exp", e);
		
	}

	int GetExp(){
		return PlayerPrefs.GetInt ("exp");
	}

	int GetTargetExp(){
		return target_exp [GetLevel() + 1];
	}
}
