﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Manager : MonoBehaviour {

	public static Manager instance;
	public float ggeemm;
	[Header("GameObject")]
	public GameObject[] lanes;
	public GameObject frame;
	public GameObject line2;
	public GameObject camMain;
	public GameObject rate;
	public GameObject bg;
	public GameObject perfect;
	public GameObject parentTempo;
	public GameObject targetGem;
	public Tempo[] tempos;

	[Header("UI")]
	public Text rateText;
	public int combo;
	public int score;

	[Header("Anim")]
	public Animator rateAnim;
	public Animator frameAnim;
	public Animator giftFrameAnim;
	public Animator[] objectes;
	public Animator gemAnim;
	public Animator starOverLayAnim;
	public Animator kingOverLayAnim;
	public Animator rightRateAnim;
	public Text     rightRateText;

	[Header("Int")]
	string currentRate;
	public int gem;
	private int numberper;
	private int numbermi;
	public int miss;

	[Header("Bool")]
	public bool isCanTouch;
	public bool isGameover;
	public bool isResetData;
	public bool isNeverDie;
	public bool isPaused;

	[Header("StarInGame")]
	public GameObject pos1;
	public Image[] stars;
	public Image[] kings;
	public RectTransform canvas;
	public Animator[] stars_anim;
	public Animator[] kings_anim;

	public StoreTheme storeTheme;


	public enum StateGame{
		playing,
		pausing
	}

	public StateGame stateGame;

	void Awake(){
		if (isResetData) {
			PlayerPrefs.DeleteAll ();
		}
		instance = this;
		Application.targetFrameRate = 60;

//		Debug.Log (stars [0].rectTransform.position + " _ " + stars [0].rectTransform.po);
	}
		

	void Start(){

		storeTheme.BtnSelect (PlayerPrefs.GetInt ("mytheme"));


		//		Time.captureFramerate = 100;
		if (PlayerPrefs.GetInt ("isplaythefirsttime") == 0) {
			PlusLive (20);
//			PlusGem (5000);
//			PlusGift (500);
			PlayerPrefs.SetInt ("numtapgem", 300);
			PlayerPrefs.SetInt ("sumperfectgem", 300);
			PlayerPrefs.SetInt ("theme0", 1);
			PlayerPrefs.SetInt ("isplaythefirsttime",1);
			PlayerPrefs.SetInt ("lastsong", 0);
			PlayerPrefs.SetString ("modelastsong", "classic");
			PlayerPrefs.SetString ("namelastsong", Koreo.instance.classicPanel.nameSongs_Classic [0]);
		}


		for (int i = 0; i < bg.transform.childCount; i++) {
			objectes [i] = bg.transform.GetChild (i).gameObject.GetComponent<Animator> ();
		}

		tempos = new Tempo[parentTempo.transform.childCount];
		int j = tempos.Length;
		for(int i = 0 ; i < j ;i++){
			tempos [i] = parentTempo.transform.GetChild (i).GetComponent<Tempo> ();
		}

		UIManager.instance.gemText.text = PlayerPrefs.GetInt ("gem").ToString ();

		PlusGem (0);
	}


	int g;
	Text comboText;
	public void Rate(string type){
		if (isGameover)
			return;




		if (type == "Perfect" || type == "Good") {
			if (type == "Perfect") {
				RewardManager.instance.Set_Sum_Of_Perfect (1);
			}
			RewardManager.instance.Set_Number_Of_Tap (1);
			if (Koreo.instance.isGiftEvent) {
				GiftPooler.instance.GetObjectPerfect ();
				PlusGift (1);
				Koreo.instance.numGift++;
				Koreo.instance.giftText.text =  "x" +Koreo.instance.numGift;
				giftFrameAnim.SetTrigger ("Frame");

				frameAnim.SetTrigger ("Frame");

			} else {
				GameObject comboObject = RatePooler.instance.GetObject();
				comboText = comboObject.GetComponent<Text> ();
				combo++;
				comboText.text = "Combo x" + combo;
				for (int i = 1; i < 20; i++) {
					if (combo == i * 100) {
						RewardManager.instance.Set_Combo_x_100 (1);
					}
				}



				numberper++;
				numbermi = 0;

//				for (int i = 1; i < 5; i++) {
//					if (numberper == 10 * i) {
//						if (rate.transform.localScale.x < 1.3f)
//							rate.transform.localScale += new Vector3 (.1f, 0, 0);
//						if (miss < 3)
//							miss++;
//					}
//				}

				UIManager.instance.missText.text = "Miss :" + miss;
				GameObject shadowObject = ShadowPooler.instance.GetObject ();
				Text shadowText = shadowObject.GetComponent<Text> ();
				RateText (combo);
				frameAnim.SetTrigger ("Frame");

				shadowText.text = currentRate;
				bool isPerfect;
				if (type == "Perfect") {
					score+=2;
					rightRateText.text = "Perfect";
					rightRateAnim.SetTrigger ("Rate");
//					PlusGem (3);
					isPerfect = true;
				} else {
					score++;
					rightRateText.text = "Good";
					rightRateAnim.SetTrigger ("Rate");
//					PlusGem (2);
					isPerfect = false;
				}
				StarPooler.instance.GetObject((int)Koreo.instance.level,isPerfect);
				SetBestScore (score);
				UIManager.instance.scoreText_ingame.text = score.ToString ();
				int gemfixed = score - g;
				g = score;
			}
		}else {
			if (Koreo.instance.isGiftEvent) {
				return;

			} else {
				isGameover = true;

				numberper = 0;
				numbermi--;
//				if (rate.transform.localScale.x > .7f) {
//					rate.transform.localScale -= new Vector3 (.1f, 0, 0);
//				}
				combo = 0;
				GameObject comboObject = RatePooler.instance.GetObject();
				comboText = comboObject.GetComponent<Text> ();
				comboText.text = type;
				miss--;
				UIManager.instance.missText.text = "Miss :" + miss;
				if (!isNeverDie && miss <= 0) {
					Revive ();
				}

			}
			return;
		}


	}
		

	public void Refresh(){
//		rate.transform.localScale = new Vector3 (1f, 1f, 1f);
		isGameover = false;
		miss = 1;
		UIManager.instance.missText.text = "Miss :" + miss;
		if (Koreo.instance.mode == "classic") {
			UIManager.instance.gemForRevive = 20;
			UIManager.instance.gemForReviveText.text = UIManager.instance.gemForRevive.ToString ();
		} else {
			UIManager.instance.gemForRevive = 200;
			UIManager.instance.gemForReviveText.text = UIManager.instance.gemForRevive.ToString ();
		}
		g = 0;
		combo = 0;
		score = 0;
		UIManager.instance.scoreText_ingame.text = score.ToString ();



	}

	public void RateText(int combo){
		if (combo <= 10) {
			rateText.text = "Cool";
		} else if (combo <= 20) {
			rateText.text = "Good";
		} else if (combo <= 30) {
			rateText.text = "Nice";
		} else if (combo <= 40) {
			rateText.text = "Great";
		} else if (combo <= 50) {
			rateText.text = "Perfect";
		} else if (combo <= 60) {
			rateText.text = "Excellent";
		} else if (combo <= 80) {
			rateText.text = "Rampage";
		} else if (combo <= 100) {
			rateText.text = "Fantastic";
		} else if (combo <= 120) {
			rateText.text = "Unbelievable";
		} else if (combo <= 500) {
			rateText.text = "God Like";
		} else {
			rateText.text = "God Like";

		}
		currentRate = rateText.text;
		rateAnim.SetTrigger ("Rate");
		float valueFixed = (Koreo.instance.valueCurve*400);
//		Debug.Log (valueFixed);
		Manager.instance.Tempos(valueFixed);
	}



	public void Tempos(float temp){
//		Debug.Log ("temp" + temp);
		int j = tempos.Length;
		for (int i = 0; i < j; i++) {
			if (i < 4) {
				tempos [i].RunAnim (10,temp/3);
			} else if (i > j - 4) {
				tempos [i].RunAnim (10,temp/3);
			} else {
				tempos [i].RunAnim (temp/3,temp/1.5f);
			}
//			if (i < 10) {
//				tempos [i].RunAnim (-35, temp * i);
//			} else {
//				tempos [i].RunAnim (-35, temp * i);
//			}
//			if (i == 0) {
//				tempos [i].RunAnim (-35, temp);
//			} else if (i == 1) {
//				tempos [i].RunAnim (-35, temp);
//			}else if (i == 2) {
//				tempos [i].RunAnim (-35, temp);
//			}else if (i == 3) {
//				tempos [i].RunAnim (-35, temp);
//			}else if (i == 4) {
//				tempos [i].RunAnim (-35, temp);
//			}else if (i == 5) {
//				tempos [i].RunAnim (-35, temp);
//			}else if (i == 6) {
//				tempos [i].RunAnim (-35, temp);
//			}else if (i == 7) {
//				tempos [i].RunAnim (-35, temp);
//			}else if (i == 8) {
//				tempos [i].RunAnim (-35, temp);
//			}else if (i == 9) {
//				tempos [i].RunAnim (-35, temp);
//			}else if (i == 10) {
//				tempos [i].RunAnim (-35, temp);
//			}else if (i == 11) {
//				tempos [i].RunAnim (-35, temp);
//			}else if (i == 12) {
//				tempos [i].RunAnim (-35, temp);
//			}else if (i == 13) {
//				tempos [i].RunAnim (-35, temp);
//			}else if (i == 14) {
//				tempos [i].RunAnim (-35, temp);
//			}
		}
	}

	public void Revive(){
		
		UIManager.instance.btnPause.SetActive (false);
		isCanTouch = false;
		Koreo.instance.audioSource.Pause ();
		UIManager.instance.Revive ();
	}

	public void GameOver(){
		StartCoroutine (gameOver ());

	}

	IEnumerator gameOver(){
		isGameover = true;
		Debug.Log (" GAME OVER ");
		Koreo.instance.audioSource.Play ();
		iTween.AudioTo(Koreo.instance.gameObject,iTween.Hash("audiosource",Koreo.instance.audioSource,"pitch",0,"time",1.5f));
		yield return new WaitForSeconds (1.5f);
		NotePooler.instance.MissNote ();
		yield return new WaitForSeconds (.1f);
		UIManager.instance.GameOver ();
	}

	public void PlusGem(float num){
		float g = PlayerPrefs.GetFloat ("gem");
		g += num;
		PlayerPrefs.SetFloat ("gem", g);
		UIManager.instance.gemText.text = ((int)g).ToString ();
		int r = Random.Range (0, 3);
		if(r == 0)
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.coinpop);
		if (num == 0)
			return;
		UIManager.instance.gemPlusText.text = "+" + num;
		UIManager.instance.gemPlusAnim.SetTrigger ("active");
	}

	public void PlusGem2(float num){
		float g = PlayerPrefs.GetFloat ("gem");
		g += num;
		PlayerPrefs.SetFloat ("gem", g);
		UIManager.instance.gemText.text = ((int)g).ToString ();
	}

	public void MinusGem(float num){
		float g = PlayerPrefs.GetFloat ("gem");
		g -= num;
		PlayerPrefs.SetFloat ("gem", g);
		UIManager.instance.gemText.text = ((int)g).ToString ();
		if (num == 0)
			return;
		UIManager.instance.gemPlusText.text = "-" + num;
		UIManager.instance.gemPlusAnim.SetTrigger ("active");
	}

	public void PlusLive(int num){
		int l = PlayerPrefs.GetInt ("live");
		l += num;
		PlayerPrefs.SetInt ("live", l);
		UIManager.instance.liveText.text = l.ToString ();
		if (num == 0)
			return;
		UIManager.instance.plusLifeText.text = "+" + num;
		UIManager.instance.plusLifeAnim.SetTrigger ("active");
	}

	public void MinusLive(int num){
		int l = PlayerPrefs.GetInt ("live");
		l -= num;
		PlayerPrefs.SetInt ("live", l);
		UIManager.instance.liveText.text = l.ToString ();
		if (num == 0)
			return;
		UIManager.instance.plusLifeText.text = "-" + num;
		UIManager.instance.plusLifeAnim.SetTrigger ("active");
	}

	public void PlusGift(int num){
		int l = PlayerPrefs.GetInt ("gift");
		l += num;
		PlayerPrefs.SetInt ("gift", l);
		UIManager.instance.giftText.text = l.ToString ();
		if (num == 0)
			return;
		UIManager.instance.giftPlusText.text = "+" + num;
		UIManager.instance.giftPlusAnim.SetTrigger ("active");
	}

	public void MinusGift(int num){
		int l = PlayerPrefs.GetInt ("gift");
		l -= num;
		PlayerPrefs.SetInt ("gift", l);
		UIManager.instance.giftText.text = l.ToString ();
		if (num == 0)
			return;
		UIManager.instance.giftPlusText.text = "-" + num;
		UIManager.instance.giftPlusAnim.SetTrigger ("active");
	}
		

	public Vector3 pos_2_Star(){
		for (int i = 0; i < stars.Length; i++) {
			if (stars [i].fillAmount < 1) {
				return stars[i].transform.position;
			}
		}
		return stars [2].transform.position;
	}

	public Vector3 pos_2_King(){
		for (int i = 0; i < kings.Length; i++) {
			if (kings [i].fillAmount < 1) {
				return kings [i].transform.position;
			}
		}
		return kings [kings.Length - 1].transform.position;
//		RectTransformUtility.PixelAdjustPoint(
	}


	public void CompleteStar(int num){
		stars_anim [num].SetTrigger ("complete");
	}

	public void CompleteKing(int num){
		kings_anim [num].SetTrigger ("complete");
	}

	public int BestScore(){
		return PlayerPrefs.GetInt ("bestscore"+Koreo.instance.numSong+ Koreo.instance.mode);
	}

	public void SetBestScore(int score){
		if (score < BestScore ())
			return;
		
		PlayerPrefs.SetInt ("bestscore"+Koreo.instance.numSong+ Koreo.instance.mode,score);
	}

	public void Update(){
		ggeemm = PlayerPrefs.GetFloat ("gem");
	}
}
