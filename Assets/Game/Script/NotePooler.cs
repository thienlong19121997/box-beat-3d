﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotePooler : MonoBehaviour {

	public static NotePooler instance;

	public GameObject objectPrefab;
	public List<GameObject> objectList = new List<GameObject> ();
	public List<Note2> note2List = new List<Note2>();
	private Vector3 currentPosition;


	public FrameControl frameControl;
	public Koreo koreo;
	public List<Obj> objList = new List<Obj>();
	public GameObject parentObj;
	void Awake () {
		instance = this;
		Generate ();
	}

	void Generate(){
		for (int i = 0; i < 50; i++) {
			GameObject objectClone = Instantiate (objectPrefab) as GameObject;
			objectClone.transform.SetParent (transform);
			objectClone.SetActive (false);
			note2List.Add (objectClone.GetComponent<Note2> ());
			objectList.Add (objectClone);
		}
		int childCout = parentObj.transform.childCount;
		for (int i = 0; i <childCout; i++) {
			objList.Add(parentObj.transform.GetChild(i).GetComponent<Obj>());
		}

	}


	public GameObject GetObject(){
		for (int i = 0; i < objectList.Count; i++) {
			if (objectList [i].activeInHierarchy == false) {
				return objectList [i];
			}
		}

		GameObject objectClone = Instantiate (objectPrefab) as GameObject;
		objectClone.transform.SetParent (transform);
		objectList.Add (objectClone);
		note2List.Add (objectClone.GetComponent<Note2> ());
		return objectClone;
	}

	public void Refresh(){
		for (int i = 0; i < objectList.Count; i++) {
			objectList [i].SetActive (false);
		}
	}

	public void MissNote(){
		for (int i = 0; i < objectList.Count; i++) {
			if(objectList[i].activeInHierarchy)
			objectList [i].GetComponent<Note2> ().MissNote ();
		}
	}

	void Update(){
		
		koreo.UpdateStep ();

		int notecout = objectList.Count;
		for (int i = 0; i < notecout; i++) {
			if (objectList [i].activeInHierarchy) {
				note2List [i].UpdateStep ();
			}
		}

		int objcout = objList.Count;
		for (int i = 0; i < objcout; i++) {
			objList [i].UpdateStep ();
		}
	
	}



}
