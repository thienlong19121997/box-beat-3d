﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RatePooler : MonoBehaviour {
	public static RatePooler instance;

	public GameObject objectPrefab;
	public List<GameObject> objectList = new List<GameObject> ();

	void Awake () {
		instance = this;
		Generate ();
	}

	void Generate(){
		for (int i = 0; i < 30; i++) {
			GameObject objectClone = Instantiate (objectPrefab) as GameObject;
			objectClone.transform.SetParent (transform);
			objectClone.SetActive (false);
			objectList.Add (objectClone);
		}
	}

	public GameObject GetObject(){
		for (int i = 0; i < objectList.Count; i++) {
			if (objectList [i].activeInHierarchy == false) {
				objectList [i].SetActive (true);
				return objectList [i];
			}
		}

		GameObject objectClone = Instantiate (objectPrefab) as GameObject;
		objectClone.transform.SetParent (transform);
		objectClone.SetActive (true);
		objectList.Add (objectClone);
		return objectClone;
	}
}
