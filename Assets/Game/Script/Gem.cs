﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gem : MonoBehaviour {

	public Vector3 currentPos;

	void Awake(){
		transform.position =new Vector2 (100, 100);
	}

	void OnEnable(){

//		transform.localPosition = currentPos;
		transform.position = new Vector2 (100, 100);;
		transform.localScale = Vector2.one;
		Invoke ("Animation", .01f);
	}

	void Animation(){
		Vector3 startPos = currentPos;
		Vector3 endPos = Manager.instance.targetGem.transform.position;
		Vector3[] path = new Vector3[2];
		path [0] = startPos + new Vector3((float)Random.Range(-500, 500)/100f,(float) Random.Range(-100,-500)/100f,0);
		path [1] = endPos;
		transform.position = startPos;
		float r = Random.Range (18, 22);
		float rf = r / 10;
		iTween.MoveTo (gameObject, iTween.Hash ("path",path,"time",rf,"easetype",iTween.EaseType.easeOutSine,"oncomplete","Complete"));
		float scale = (float)Random.Range(70f,100f)/100f;
		transform.localScale = new Vector3 (scale, scale, scale);
		iTween.ScaleTo(gameObject,iTween.Hash("scale",new Vector3(.6f,.6f,.6f),"time",rf,"easetype",iTween.EaseType.easeInSine));
	}

	void Complete(){
		Debug.Log ("complete");
//		SoundManager.instance.PlaySound (SoundManager.instance.coin);
		Manager.instance.gemAnim.SetTrigger ("Highlight");
		Manager.instance.PlusGem (10);
		gameObject.SetActive (false);
		transform.position = new Vector3(-100,-100,0);
		transform.localScale = Vector3.one;
	}

	void Refresh(){

	}


}
