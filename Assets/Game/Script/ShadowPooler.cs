﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowPooler : MonoBehaviour {
	public static ShadowPooler instance;

	public GameObject objectPrefab;
	public List<GameObject> objectList = new List<GameObject> ();
	void Awake () {
		instance = this;
		Generate ();

	}

	void Generate(){
		for (int i = 0; i < 30; i++) {
			GameObject objectClone = Instantiate (objectPrefab) as GameObject;
			objectClone.transform.SetParent (transform);
			objectClone.SetActive (false);
			objectClone.GetComponent<RectTransform>().anchoredPosition3D = Vector3.zero;
			objectList.Add (objectClone);
		}
	}

	public GameObject GetObject(){
		for (int i = 0; i < objectList.Count; i++) {
			if (objectList [i].activeInHierarchy == false) {
				objectList [i].SetActive (true);
				objectList [i].GetComponent<RectTransform>().anchoredPosition3D = Vector3.zero;
				return objectList [i];
			}
		}

		GameObject objectClone = Instantiate (objectPrefab) as GameObject;
		objectClone.transform.SetParent (transform);
		objectClone.SetActive (true);
		objectList.Add (objectClone);
		objectClone.GetComponent<RectTransform>().anchoredPosition3D = Vector3.zero;
		return objectClone;
	}
}
