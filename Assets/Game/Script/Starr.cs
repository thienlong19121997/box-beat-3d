﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Starr : MonoBehaviour {

	Vector3 currentScale;

	Vector3[] path = new Vector3[2];
	Vector3 pos0,pos1;
	public string name;
	int number;
	Vector3 targetPosition;
	void Awake(){
		pos1 = Manager.instance.pos1.transform.position;
		currentScale = transform.localScale;
	}



	public void RunAnim(Vector3 pos2,bool isPerfect){
		InGame.instance.Update_Star_King ((int)Koreo.instance.level,isPerfect);


		targetPosition = pos2;

		iTween.StopByName (name);

		gameObject.SetActive (true);

		pos0 = (Vector3)RectTransformUtility.WorldToScreenPoint(Camera.main, Manager.instance.frame.transform.position);
		transform.position = pos0;

		path [0] =  Manager.instance.pos1.transform.position;
		path [1] = pos2;

		iTween.MoveTo (gameObject, iTween.Hash ("name",name,"path", path, "time", 1.6f, "easetype", iTween.EaseType.easeOutSine, "oncomplete", "OnComplete"));
		transform.localScale = new Vector3 (3.0f, 3.0f, 3.0f);
		iTween.ScaleTo (gameObject, iTween.Hash ("name",name,"scale", new Vector3 (3.21f*.3f,3.21f*.3f,3.21f*.3f), "time", 1.4f, "easetype", iTween.EaseType.easeInQuart));
	}

	void OnComplete(){
		int level = (int)Koreo.instance.level;

		GameObject animClone;
		if (level == 0) {
			animClone = StarKingPooler.instance.GetObjectStar ();
		} else {
			animClone = StarKingPooler.instance.GetObjectKing ();
		}
		animClone.transform.position = targetPosition;


		gameObject.SetActive (false);

	}




}
