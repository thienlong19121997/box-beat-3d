﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarPooler : MonoBehaviour {

	public static StarPooler instance;

	public GameObject prefabs;
	public List<GameObject> listObject = new List<GameObject>();

	public GameObject prefabs2;
	public List<GameObject> listObject2 = new List<GameObject>();

	int id;
	// Use this for initialization
	void Awake () {
		instance = this;
	}

	// Update is called once per frame
	void Start () {
		SpawnObject ();
	}

	void SpawnObject(){
		for(int i = 0 ; i < 30 ; i++){
			GameObject objectClone = Instantiate (prefabs) as GameObject;
			objectClone.transform.SetParent (transform);
			objectClone.SetActive (false);
			listObject.Add (objectClone);
			objectClone.GetComponent<Starr> ().name = objectClone.gameObject.name + id;
			id++;
			GameObject object2Clone = Instantiate (prefabs2) as GameObject;
			object2Clone.transform.SetParent (transform);
			object2Clone.SetActive (false);
			listObject2.Add (object2Clone);
			object2Clone.GetComponent<Starr> ().name = objectClone.gameObject.name + id;
			id++;
		}
	}

	public GameObject GetObject(int level,bool isPerfect){
		if (level == 0) {
			for (int i = 0; i < listObject.Count; i++) {
				if (listObject [i].activeInHierarchy == false) {
			
					listObject[i].GetComponent<Starr> ().RunAnim (Manager.instance.pos_2_Star(),isPerfect);
					
					return listObject [i];
				}
			}

			GameObject objectClone = Instantiate (prefabs) as GameObject;
			objectClone.transform.SetParent (transform);
		
			objectClone.GetComponent<Starr> ().RunAnim (Manager.instance.pos_2_Star(),isPerfect);
			
			listObject.Add (objectClone);

			return objectClone;
		} else {
			for (int i = 0; i < listObject2.Count; i++) {
				if (listObject2 [i].activeInHierarchy == false) {

					listObject2[i].GetComponent<Starr> ().RunAnim (Manager.instance.pos_2_King(),isPerfect);

					return listObject2 [i];
				}
			}

			GameObject object2Clone = Instantiate (prefabs2) as GameObject;
			object2Clone.transform.SetParent (transform);

			object2Clone.GetComponent<Starr> ().RunAnim (Manager.instance.pos_2_King(),isPerfect);

			listObject2.Add (object2Clone);

			return object2Clone;
		}


	}

	public void Refresh(){
		for (int i = 0; i < listObject.Count; i++) {
			listObject [i].SetActive (false);
//			listObject [i].transform.localScale = Vector2.zero;
			listObject2 [i].SetActive (false);
		}
	}
}
