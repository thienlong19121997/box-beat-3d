﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiftPooler : MonoBehaviour {

	public static GiftPooler instance;

	public GameObject objectPrefabs;
	public List<GameObject> objectList = new List<GameObject> ();


	void Awake () {
		instance = this;
		Generate ();
	}

	void Generate(){
		for (int i = 0; i < 30; i++) {
			GameObject objectClone = Instantiate (objectPrefabs) as GameObject;
			objectClone.transform.SetParent (transform);
			objectClone.SetActive (false);
			objectClone.GetComponent<RectTransform> ().anchoredPosition3D = Vector3.zero;
			objectList.Add (objectClone);

		}
	}


	public GameObject GetObjectPerfect(){
		for (int i = 0; i < objectList.Count; i++) {
			if (objectList [i].activeInHierarchy == false) {
				objectList [i].SetActive (true);
				return objectList [i];
			}
		}

		GameObject objectClone = Instantiate (objectPrefabs) as GameObject;
		objectClone.transform.SetParent (transform);
		objectClone.SetActive (true);
		objectList.Add (objectClone);
		return objectClone;
	}

}
