﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelUpPanel : MonoBehaviour {

	public GameObject content;


	[Header("Classic")]
	public ClassicPanel classicPanel;

	public GameObject[] btnPlay_classic;
	public GameObject[] btnBuy_classic;

	public Text[] stt_songs_classic;
	public Text[] name_songs_classic;
	public Text levelText_classic;

	public GameObject[] btnTry_classic;
	public GameObject[] btnStopTry_classic;

	[Header("Modern")]
	public GameObject modern;
	public ModernPanel modernPanel;

	public GameObject btnPlay_modern;
	public GameObject btnBuy_modern;

	public Text stt_songs_modern;
	public Text name_songs_modern;
	public Text levelText_modern;

	public GameObject btnTry_modern;
	public GameObject btnStopTry_modern;

	[Header("Orchestra")]
	public GameObject orchestra;
	public OrchestraPanel orchestraPanel;

	public GameObject btnPlay_orchestra;
	public GameObject btnBuy_orchestra;

	public Text stt_songs__orchestra;
	public Text name_songs__orchestra;
	public Text levelText__orchestra;

	public GameObject btnTry_orchestra;
	public GameObject btnStopTry_orchestra;

	[Header("Edm")]
	public GameObject edm;
	public EDMPanel eDMPanel;

	public GameObject btnPlay_edm;
	public GameObject btnBuy_edm;

	public Text stt_songs_edm;
	public Text name_songs_edm;
	public Text levelText_edm;

	public GameObject btnTry_edm;
	public GameObject btnStopTry_edm;

	public int level;

	void OnEnable(){
		BtnStopTry (false);

		content.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, -228.45f);

		btnPlay_modern.SetActive (false);
		btnPlay_orchestra.SetActive (false);
		btnPlay_edm.SetActive (false);
		btnPlay_edm .GetComponent<Button> ().onClick.RemoveAllListeners ();
		btnPlay_orchestra .GetComponent<Button> ().onClick.RemoveAllListeners ();
		btnPlay_modern .GetComponent<Button> ().onClick.RemoveAllListeners ();

		btnBuy_modern.SetActive (true);
		btnBuy_orchestra.SetActive (true);
		btnBuy_edm.SetActive (true);
		btnBuy_edm .GetComponent<Button> ().onClick.RemoveAllListeners ();
		btnBuy_orchestra .GetComponent<Button> ().onClick.RemoveAllListeners ();
		btnBuy_modern .GetComponent<Button> ().onClick.RemoveAllListeners ();

		for (int i = 0; i < 4; i++) {
			btnPlay_classic [i].SetActive (false);
			btnBuy_classic [i].SetActive (true);
			btnPlay_classic [i].GetComponent<Button> ().onClick.RemoveAllListeners ();
			btnBuy_classic  [i].GetComponent<Button> ().onClick.RemoveAllListeners ();

		}

		level = PlayerPrefs.GetInt ("level");

		if (level <= 16) {
			stt_songs_edm.text = (level).ToString ();
			stt_songs__orchestra.text = (level).ToString ();
			stt_songs_modern.text = (level).ToString ();
			name_songs_edm.text = eDMPanel.nameSongs_edm [level];
			name_songs__orchestra.text = orchestraPanel.nameSongs_orchestra [level];
			name_songs_modern.text = modernPanel.nameSongs_modern [level];
		}

		int fr = PlayerPrefs.GetInt ("last_stt")+1;
		int to  = PlayerPrefs.GetInt ("last_stt")+4;
		for (int i = fr; i <= to; i++) {
			stt_songs_classic [i-fr].text = (i).ToString ();
			name_songs_classic [i-fr].text = classicPanel.nameSongs_Classic [i];
			PlayerPrefs.SetInt ("last_stt", i);
			int number = i;
			btnPlay_classic [i-(fr)].GetComponent<Button> ().onClick.AddListener (delegate() {
				BtnPlayClassic(number);
			});
			btnBuy_classic [i-(fr)].GetComponent<Button> ().onClick.AddListener (delegate() {
				BtnBuyClassic(number);
			});
			btnTry_classic [i-(fr)].GetComponent<Button> ().onClick.AddListener (delegate() {
				BtnTry_Classic(number);
			});
			btnStopTry_classic [i-(fr)].GetComponent<Button> ().onClick.AddListener (delegate() {
				BtnStopTry(false);
			});
		}
		if (level >= eDMPanel.nameSongs_edm.Length) {
			edm.SetActive (false);
			orchestra.SetActive (false);
			edm.SetActive (false);
		} else {
			edm.SetActive (true);
			orchestra.SetActive (true);
			edm.SetActive (true);
		}

		btnPlay_modern .GetComponent<Button> ().onClick.AddListener (delegate() {
			BtnPlayModern(level);
		});
		btnBuy_modern .GetComponent<Button> ().onClick.AddListener (delegate() {
			BtnBuyModern(level);
		});

		btnPlay_orchestra .GetComponent<Button> ().onClick.AddListener (delegate() {
			BtnPlayOrchestra(level);
		});
		btnBuy_orchestra .GetComponent<Button> ().onClick.AddListener (delegate() {
			BtnBuyOrchestra(level);
		});

		btnPlay_edm .GetComponent<Button> ().onClick.AddListener (delegate() {
			BtnPlayEdm(level);
		});
		btnBuy_edm .GetComponent<Button> ().onClick.AddListener (delegate() {
			BtnBuyEdm(level);
		});

		btnTry_modern .GetComponent<Button> ().onClick.AddListener (delegate() {
			BtnTry_Modern(level);
		});
		btnStopTry_modern .GetComponent<Button> ().onClick.AddListener (delegate() {
			BtnStopTry(false);
		});

		btnTry_orchestra .GetComponent<Button> ().onClick.AddListener (delegate() {
			BtnTry_Orchestra(level);
		});
		btnStopTry_orchestra .GetComponent<Button> ().onClick.AddListener (delegate() {
			BtnStopTry(false);
		});

		btnTry_edm .GetComponent<Button> ().onClick.AddListener (delegate() {
			BtnTry_Edm(level);
		});
		btnStopTry_edm .GetComponent<Button> ().onClick.AddListener (delegate() {
			BtnStopTry(false);
		});

	}

	public void BtnBuyClassic(int num){
//		Debug.Log (num);
		if (PlayerPrefs.GetFloat ("gem") < 200) {

			return;
		}

		Manager.instance.MinusGem (200);
		PlayerPrefs.SetInt ("active_classic_song" + num, 1);
		UpdateSongClassic ();
	}

	public void BtnPlayClassic(int num){
		Debug.Log (num);
		Koreo.instance.mode = "classic";
		UIManager.instance.BtnPlayGame (num);
	}

	void UpdateSongClassic(){
		int from = PlayerPrefs.GetInt ("last_stt")+1-4;
		int to  = PlayerPrefs.GetInt ("last_stt")+4-4;
		for (int i = from; i <= to; i++) {
//			Debug.Log (i);
			if (PlayerPrefs.GetInt ("active_classic_song" + i) == 1) {
				btnBuy_classic [i - from].SetActive (false);	
				btnPlay_classic [i - from].SetActive (true);
			} else {
				btnBuy_classic [i - from].SetActive (true);	
				btnPlay_classic [i - from].SetActive (false);
			}
		}
	}

	public void BtnBuyModern(int num){
//		Debug.Log (num);
		if (PlayerPrefs.GetFloat ("gem") < 2000) {

			return;
		}

		Manager.instance.MinusGem (2000);
		PlayerPrefs.SetInt ("active_modern_song" + num, 1);
		UpdateSongModern ();
	}

	public void BtnPlayModern(int num){
//		Debug.Log (num);
		Koreo.instance.mode = "modern";
		UIManager.instance.BtnPlayGame (num);
	}

	public void UpdateSongModern(){
		int level = PlayerPrefs.GetInt ("level");

		if (PlayerPrefs.GetInt ("active_modern_song" + level) == 1) {
			btnBuy_modern.SetActive (false);
			btnPlay_modern.SetActive (true);
		} else {
			btnBuy_modern.SetActive (true);
			btnPlay_modern.SetActive (false);
		}

	}



	public void BtnBuyOrchestra(int num){
//		Debug.Log (num);
		if (PlayerPrefs.GetFloat ("gem") < 2000) {

			return;
		}

		Manager.instance.MinusGem (2000);
		PlayerPrefs.SetInt ("active_orchestra_song" + num, 1);
		UpdateSongOrchestra ();
	}

	public void BtnPlayOrchestra(int num){
//		Debug.Log (num);
		Koreo.instance.mode = "orchestra";
		UIManager.instance.BtnPlayGame (num);
	}

	public void UpdateSongOrchestra(){
		int level = PlayerPrefs.GetInt ("level");

		if (PlayerPrefs.GetInt ("active_orchestra_song" + level) == 1) {
			btnBuy_orchestra.SetActive (false);
			btnPlay_orchestra.SetActive (true);
		} else {
			btnBuy_orchestra.SetActive (true);
			btnPlay_orchestra.SetActive (false);
		}

	}



	public void BtnBuyEdm(int num){
//		Debug.Log (num);
		if (PlayerPrefs.GetFloat ("gem") < 2000) {

			return;
		}

		Manager.instance.MinusGem (2000);
		PlayerPrefs.SetInt ("active_edm_song" + num, 1);
		UpdateSongEdm ();
	}

	public void BtnPlayEdm(int num){
//		Debug.Log (num);
		Koreo.instance.mode = "edm";
		UIManager.instance.BtnPlayGame (num);
	}

	public void UpdateSongEdm(){
		int level = PlayerPrefs.GetInt ("level");

		if (PlayerPrefs.GetInt ("active_edm_song" + level) == 1) {
			btnBuy_edm.SetActive (false);
			btnPlay_edm.SetActive (true);
		} else {
			btnBuy_edm.SetActive (true);
			btnPlay_edm.SetActive (false);
		}
	}

	public void BtnTry_Classic(int num){
		BtnStopTry (false);

		BackgroundMusic.instance.OnClickTrySong_Classic (num);
		Debug.Log(num + " ___ " + PlayerPrefs.GetInt("last_stt"));
		btnStopTry_classic [num+3-PlayerPrefs.GetInt("last_stt")].SetActive (true);
	}

	public void BtnTry_Modern(int num){
		BtnStopTry (false);

		BackgroundMusic.instance.OnClickTrySong_Modern (num);
		btnStopTry_modern.SetActive (true);
	}
		
	public void BtnTry_Orchestra(int num){
		BtnStopTry (false);

		BackgroundMusic.instance.OnClickTrySong_Orchestra (num);
		btnStopTry_orchestra.SetActive (true);
	}

	public void BtnTry_Edm(int num){
		BtnStopTry (false);

		BackgroundMusic.instance.OnClickTrySong_Edm (num);
		btnStopTry_edm.SetActive (true);
	}

	public void BtnStopTry(bool isActive){
		BackgroundMusic.instance.OnClickStopTrySong ();
		btnStopTry_classic [0].SetActive (isActive);
		btnStopTry_classic [1].SetActive (isActive);
		btnStopTry_classic [2].SetActive (isActive);
		btnStopTry_classic [3].SetActive (isActive);
		btnStopTry_modern.SetActive (isActive);
		btnStopTry_orchestra.SetActive (isActive);
		btnStopTry_edm.SetActive (isActive);
	}
}
