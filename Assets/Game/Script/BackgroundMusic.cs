﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMusic : MonoBehaviour {
	public static BackgroundMusic instance;

	public AudioClip[] songsClip_classic;
	public AudioClip[] songsClip_modern;
	public AudioClip[] songsClip_orchestra;
	public AudioClip[] songsClip_edm;

	public AudioSource audioSource_lastsong;
	AudioSource audioSource;

	const float timeStop = 15.0f;
	public float timePlaying;
	public bool isStop;

	public ClassicPanel classicPanel;
	public ModernPanel modernPanel;
	public OrchestraPanel orchestraPanel;
	public EDMPanel edmPanel;

	void Awake(){
		instance = this;
		audioSource = GetComponent<AudioSource> ();
	}


	public void OnClickTrySong_Classic(int num){
		audioSource.time = 1.5f;
		isStop = false;
		audioSource.clip = songsClip_classic [num];
		PauseLastSongMusic ();
		audioSource.Play ();
		timePlaying = timeStop;
		audioSource.time = 1.5f;

	}

	public void OnClickTrySong_Modern(int num){
		audioSource.time = 1.5f;
		PauseLastSongMusic ();	
		isStop = false;
		audioSource.clip = songsClip_modern [num];
		audioSource.Play ();
		timePlaying = timeStop;
		audioSource.time = 1.5f;

	}

	public void OnClickTrySong_Orchestra(int num){
		audioSource.time = 1.5f;
		PauseLastSongMusic ();		isStop = false;
		audioSource.clip = songsClip_orchestra [num];
		audioSource.Play ();
		timePlaying = timeStop;
		audioSource.time = 1.5f;

	}

	public void OnClickTrySong_Edm(int num){
		audioSource.time = 1.5f;
		PauseLastSongMusic ();
		isStop = false;
		audioSource.clip = songsClip_edm [num];
		audioSource.Play ();
		timePlaying = timeStop;
		audioSource.time = 1.5f;

	}

	void Update(){
		if (UIManager.instance.mainMenu.activeInHierarchy == false)
			return;
		if (timePlaying > 0) {
			timePlaying -= Time.deltaTime;
	
		} else {
			if (isStop == false) {
				StopMusic ();

			}
		}
	}

	public void StopMusic(){
		classicPanel.BtnStopTrySong ();
		modernPanel.BtnStopTrySong ();
		orchestraPanel.BtnStopTrySong ();
		edmPanel.BtnStopTrySong ();

	}

	public void OnClickStopTrySong(){
		if (isStop == false) {
			isStop = true;
			audioSource.Stop ();
		}
	}

	public void PauseLastSongMusic(){
		audioSource_lastsong.Pause ();
	}

	public void PlayMusicLastSong(){
		audioSource_lastsong.Play ();
	}

}
