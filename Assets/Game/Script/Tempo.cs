﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tempo : MonoBehaviour {

	string name;
	float speed;

	void Awake(){
		name = gameObject.name;
		speed = Random.Range (10, 60);
	}

//	void Update(){
//		transform.Rotate (new Vector3 (speed*Time.deltaTime, speed*Time.deltaTime, speed*Time.deltaTime));
//	}

	public void RunAnim(float min,float max){
//		iTween.Stop (); // temp 10->100
		iTween.StopByName(name);
		Vector3 target = new Vector3(transform.localScale.x,(float)Random.Range(min,max)/10,transform.localScale.z);
		transform.localScale = new Vector3 (transform.localScale.x, (float)Random.Range(10,30)/10, transform.position.y);
		iTween.ScaleTo(gameObject,iTween.Hash("name",name,"scale", target,"time",0.15f,"easetype",iTween.EaseType.easeOutSine,"oncomplete","EndAnim"));
//		Vector3 to = new Vector3 (transform.position.x,(float)Random.Range(max,max)/10,transform.position.z);
//		transform.localPosition = new Vector3 (transform.localPosition.x, max, transform.localPosition.z);
//		transform.position = new Vector3 (transform.position.x, (float)Random.Range (-35,0) / 100, transform.position.z);
//		iTween.MoveTo (gameObject, iTween.Hash ("name", name, "position", to, "time", .15f, "easetype", iTween.EaseType.easeOutSine, "oncomplete", "EndAnim"));
	}

	void EndAnim(){
		iTween.ScaleTo(gameObject,iTween.Hash("name",name,"scale", new Vector3(1,1f,1),"time",0.2f,"easetype",iTween.EaseType.easeOutSine));
//		iTween.MoveTo(gameObject,iTween.Hash("name",name,"position",new Vector3(transform.position.x,-3.5f,transform.position.z),"time",0.25f,"easetype",iTween.EaseType.easeOutSine));

	}
}
