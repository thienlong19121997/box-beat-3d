﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OrchestraPanel : MonoBehaviour {
	public static OrchestraPanel instance;


	public class StarScript{
		public Image[] stars_fill;
		public Image[] kings_fill;
	}


	public GameObject parent;
	private int sumChild;
	private GameObject[] songs;


	private GameObject[] star;
	private GameObject[] king;

	private StarScript[] starScript;

	private GameObject[] parentLOCK;
	private Text[] stt_songs_1;
	private List<Text> stt_songs_2 = new List<Text>();
	private Text[] stt_name_songs_1;
	private List<Text> stt_name_songs_2 = new List<Text>();

	//
	private GameObject[] btnPlay;
	private GameObject[] btnBuy;

	public string[] nameSongs_orchestra;

	public GameObject[] btnStopTrySongs;
	public GameObject[] btnTrySongs;

	void Awake(){
		instance = this;
		OnStart ();
	}

	void OnStart(){
		sumChild    = parent.transform.childCount;
		songs       = new GameObject[sumChild];
		star        = new GameObject[sumChild];
		king        = new GameObject[sumChild];
		starScript  = new StarScript[sumChild];
		stt_songs_1 = new Text[sumChild];
		stt_name_songs_1 = new Text[sumChild];
		btnPlay     = new GameObject[sumChild];	
		btnBuy      = new GameObject[sumChild];
		parentLOCK      = new GameObject[sumChild];
		btnStopTrySongs = new GameObject[sumChild];
		btnTrySongs = new GameObject[sumChild];
		for (int i = 0; i < sumChild; i++) {
			songs [i] = parent.transform.GetChild (i).gameObject;
			star [i] = songs [i].transform.GetChild(4).GetChild (1).gameObject;
			king [i] = songs [i].transform.GetChild(4).GetChild (2).gameObject;
			StarScript ss = new StarScript();
			starScript [i] = ss;
			starScript[i].stars_fill = new Image[3];
			starScript[i].kings_fill = new Image[3];
			for (int j = 0; j < 3; j++) {
				starScript [i].stars_fill [j] = star [i].transform.GetChild (j).transform.GetChild (1).GetComponent<Image> ();
				starScript [i].kings_fill [j] = king [i].transform.GetChild (j).transform.GetChild (1).GetComponent<Image> ();
			}
			btnPlay [i] = songs [i].transform.GetChild (4).gameObject;
			btnBuy  [i] = songs [i].transform.GetChild (5).gameObject;
			parentLOCK [i] = songs [i].transform.GetChild (9).gameObject;
			btnStopTrySongs [i] = songs [i].transform.GetChild (3).GetChild (2).gameObject;
			btnTrySongs [i] = songs [i].transform.GetChild (3).GetChild (0).gameObject;
			int a = i;
			btnStopTrySongs [a].GetComponent<Button> ().onClick.AddListener (delegate() {
				BtnStopTrySong();
			});
			btnTrySongs [a].GetComponent<Button> ().onClick.AddListener (delegate() {
				BtnTrySong(a);
			});
		}



		UpdateMenuHome ();
		INIT_ListSong ();
	}

	void OnEnable(){
		UpdateSong_Orchestra ();
		UpdateMenuHome ();
	}

	public void UpdateMenuHome(){
		for (int i = 0; i < sumChild; i++) {
			if (PlayerPrefs.GetInt ("mode_king_song" + i+ "orchestra") >= 1) {
				star [i].SetActive (false);
				king [i].SetActive (true);
			} else {
				king [i].SetActive (false);
				star [i].SetActive (true);
			}
			for (int j = 0; j < 3; j++) {
				starScript [i].stars_fill [j].fillAmount = PlayerPrefs.GetFloat ("starfill_"+j+"_song" + i + "orchestra");
				starScript [i].kings_fill [j].fillAmount = PlayerPrefs.GetFloat ("kingfill_"+j+"_song" + i +  "orchestra");
			}
		}
	}

	public void INIT_ListSong(){
		for (int i = 0; i < sumChild; i++) {
			stt_songs_1 [i] = songs [i].transform.GetChild (7).gameObject.GetComponent<Text> ();
			stt_name_songs_1 [i] = songs [i].transform.GetChild (6).gameObject.GetComponent<Text> ();


		}
		for (int i = 1; i < parentLOCK.Length; i++) {
			stt_songs_2.Add (parentLOCK [i].transform.GetChild (1).GetComponent<Text> ());
			stt_name_songs_2.Add (parentLOCK [i].transform.GetChild (2).GetComponent<Text> ());

		}
		//
		for (int i = 0; i < sumChild; i++) {
			stt_songs_1 [i].text = i.ToString ();
			stt_name_songs_1 [i].text = nameSongs_orchestra [i];

			if (i >= 1) {
				stt_songs_2 [i - 1].text = i.ToString ();
				stt_name_songs_2 [i - 1].text =  nameSongs_orchestra [i];
			}

		}


	}

	public void UpdateSong_Orchestra(){
		for (int i = 1; i < parentLOCK.Length; i++) {
			if (PlayerPrefs.GetInt ("level") >= i) {
				parentLOCK [i].SetActive (false);
			} else {
				parentLOCK [i].SetActive (true);
			}
		}

		for (int i = 1; i < sumChild; i++) {
			if (PlayerPrefs.GetInt ("active_orchestra_song" + i) == 1) {
				btnPlay [i].SetActive (true);
				btnBuy [i].SetActive (false);
			} else{
				btnPlay [i].SetActive (false);
				btnBuy [i].SetActive (true);
			}
		}

	}

	public void Buy_Song_Orchestra(int num){
		if (PlayerPrefs.GetFloat ("gem") < 2000) {
			UIManager.instance.BtnStore ();

			return;
		}

		Manager.instance.MinusGem (2000);
		PlayerPrefs.SetInt ("active_orchestra_song" + num, 1);
		UpdateSong_Orchestra ();
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.livepop);
	}

	public void BtnTrySong(int num){
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.button);

		BtnStopTrySong ();
		BackgroundMusic.instance.OnClickTrySong_Orchestra (num);
		btnStopTrySongs [num].SetActive (true);
	}

	public void BtnStopTrySong(){
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.button);

		BackgroundMusic.instance.PlayMusicLastSong ();
		BackgroundMusic.instance.OnClickStopTrySong ();
		for (int i = 0; i < sumChild; i++) {
			if (btnStopTrySongs [i].activeInHierarchy) {
				btnStopTrySongs [i].SetActive (false);
			}
		}
	}
}
