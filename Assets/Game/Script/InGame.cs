﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGame : MonoBehaviour {

	public static InGame instance;

//	[Header("Star")]
	public GameObject star;
	public Image[] stars_fill;
	public GameObject[] highlight_stars;
//	[Header("King")]
	public GameObject king;
	public Image[] kings_fill;
	public GameObject[] highlight_kings;
	float k;

	void Awake(){
		instance = this;
	}
	// Use this for initialization
	void Start () {
		
	}
	public void Mode_Star(){
		score = 0;
		for (int i = 0; i < stars_fill.Length; i++) {
			stars_fill [i].fillAmount = 0;
			kings_fill [i].fillAmount = 0;
			highlight_stars [i].SetActive (false);
			highlight_kings [i].SetActive (false);
		}
		Manager.instance.starOverLayAnim.SetTrigger ("highlight");
		Manager.instance.kingOverLayAnim.SetTrigger ("default");

//		star.SetActive (true);
//		king.SetActive (false);
	}
	public void Mode_King(){
//		score = 0;
//		star.SetActive (false);
//		king.SetActive (true);
		Manager.instance.starOverLayAnim.SetTrigger ("hide");
		Manager.instance.kingOverLayAnim.SetTrigger ("highlight");

	}


	float fill_0,fill_1,fill_2;
	float score;
	public void Update_Star_King(int level,bool isPerfect){
		if (level <= 0) {
			k = 0;

			if (isPerfect) {
				score++;
			} else {
				score += 0.5f;
			}
			fill_0 = score / ((Koreo.instance.sumEvent) * .25f);       // 25%
			stars_fill [0].fillAmount = fill_0;
			fill_1 = (score - (Koreo.instance.sumEvent) * .25f) / ((Koreo.instance.sumEvent) * .25f);  // 25%
			stars_fill [1].fillAmount = fill_1;
			fill_2 = (score - (Koreo.instance.sumEvent) * .5f) / ((Koreo.instance.sumEvent) * .25f); // 25%
			stars_fill [2].fillAmount = fill_2;
//			Debug.Log (  fill_0 + "_"+ fill_1+ " _ " + fill_2);
			PlayerPrefs.SetFloat ("starfill_0_song" + Koreo.instance.numSong + Koreo.instance.mode ,fill_0);
			PlayerPrefs.SetFloat ("starfill_1_song" + Koreo.instance.numSong+ Koreo.instance.mode,fill_1);
			PlayerPrefs.SetFloat ("starfill_2_song" + Koreo.instance.numSong+ Koreo.instance.mode,fill_2);
			for (int i = 0; i < highlight_stars.Length; i++) {
				if (stars_fill [i].fillAmount >= 1) {
					highlight_stars [i].SetActive (true);
				}
			}

			int percentGEM = 1;
			if (PlayerPrefs.GetInt ("level") == 0) {
				percentGEM = 2;
			}

			if (isPerfect) {
				if (stars_fill [0].fillAmount <= 1) {
					Manager.instance.PlusGem2 (120*percentGEM/(Koreo.instance.sumEvent*0.25f));                 // 120  
				} else if (stars_fill [1].fillAmount <= 1) {     									  
					Manager.instance.PlusGem2 (140/(Koreo.instance.sumEvent*0.25f));					// 260				
				} else if (stars_fill [2].fillAmount <= 1) {
					Manager.instance.PlusGem2 (80/(Koreo.instance.sumEvent*0.25f));                 // 340
				}else{
					Manager.instance.PlusGem2(1);
				}
			} else {
				if (stars_fill [0].fillAmount <= 1) {
					Manager.instance.PlusGem2 (60/(Koreo.instance.sumEvent*0.25f));                 // 60  
				} else if (stars_fill [1].fillAmount <= 1) {     									  
					Manager.instance.PlusGem2 (70/(Koreo.instance.sumEvent*0.25f));					// 130				
				} else if (stars_fill [2].fillAmount <= 1) {
					Manager.instance.PlusGem2 (40/(Koreo.instance.sumEvent*0.25f));                 // 260
				}else{
					Manager.instance.PlusGem2(1);
				}
			}
			
		} else {
			k++;

			PlayerPrefs.SetInt ("mode_king_song" + Koreo.instance.numSong+ Koreo.instance.mode, 1);
			if (kings_fill [0].fillAmount < 1) {
				float fill_0 = k / ((Koreo.instance.sumEvent)*2.9f);
				kings_fill [0].fillAmount = fill_0;
				PlayerPrefs.SetFloat ("kingfill_0_song" + Koreo.instance.numSong+ Koreo.instance.mode, fill_0);
			} else if (kings_fill [1].fillAmount < 1) {
				float fill_1 =  (k - (Koreo.instance.sumEvent)*2.9f) / ((Koreo.instance.sumEvent)*2.9f);
				kings_fill [1].fillAmount = fill_1;
				PlayerPrefs.SetFloat ("kingfill_1_song" + Koreo.instance.numSong+ Koreo.instance.mode, fill_1);
			} else if (kings_fill [2].fillAmount < 1) {
				float fill_2 =  (k - (Koreo.instance.sumEvent)*5.9f) / ((Koreo.instance.sumEvent)*2.9f);
				kings_fill [2].fillAmount = fill_2;
				PlayerPrefs.SetFloat ("kingfill_2_song" + Koreo.instance.numSong+ Koreo.instance.mode, fill_2);
			}
			for (int i = 0; i < highlight_kings.Length; i++) {
				if (kings_fill [i].fillAmount >= 1) {
					highlight_kings [i].SetActive (true);
				}
			}
		}
	}



	public void Refresh(){

	}
}
