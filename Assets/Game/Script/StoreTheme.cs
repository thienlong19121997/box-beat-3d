﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoreTheme : MonoBehaviour {


	public GameObject parent;

	public GameObject[] btnSelect;
	public GameObject[] btnSelected;
	public GameObject[] btnBuy;
	public Sprite[] themes;
	public Image bg0,bg1,bg2;

	int count;




	// Use this for initialization


	void Awake () {
		Init ();
	}

	void Init(){
		count = parent.transform.childCount;
		btnSelect = new GameObject[count];
		btnSelected = new GameObject[count];
		btnBuy = new GameObject[count];
		for (int i = 0; i < count; i++) {
			btnSelect[i] = parent.transform.GetChild (i).GetChild (3).gameObject;
			btnSelected[i] = parent.transform.GetChild (i).GetChild (4).gameObject;
			btnBuy[i] = parent.transform.GetChild (i).GetChild (5).gameObject;
		}
	}

	void OnEnable(){
		if (btnSelect [0] == null) {
			Init ();
		}
		UpdateStoreTheme ();
	}

	public void UpdateStoreTheme(){
		for (int i = 0; i < count; i++) {
			if (PlayerPrefs.GetInt ("theme" + i) == 0) {
				btnBuy [i].SetActive (true);
			} else {
				btnBuy [i].SetActive (false);

			}
		}
	}

	public void BtnSelect(int num){
		Init ();
		for (int i = 0; i < count; i++) {
			if (i == num) {
				btnSelected [i].SetActive (true);
			} else {
				btnSelected [i].SetActive (false);
			}
		}
		PlayerPrefs.SetInt ("mytheme", num);
		bg0.sprite = themes [num];
		bg1.sprite = themes [num];
		bg2.sprite = themes [num];
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.button);
	}

	public void BtnBuy(int num){
		if (PlayerPrefs.GetFloat ("gem") < 500) {
			UIManager.instance.BtnPanelGem ();
		} else {
			Manager.instance.MinusGem (500);
			PlayerPrefs.SetInt ("theme" + num,1);
			UpdateStoreTheme ();
			EffectMusic.instance.PlayOneShot (EffectMusic.instance.livepop);
		}
	}
		
	

}
