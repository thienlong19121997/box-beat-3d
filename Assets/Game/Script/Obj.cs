﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obj : MonoBehaviour {

	Vector3 currentPosition;
	float rotateSpeed;
	float x,y,z;
	Renderer rend;
	float currentZ;
	bool isMoveZ;
	void Awake(){
		rend = GetComponent<Renderer> ();
	}
	// Use this for initialization
	void Start () {
		currentZ = transform.position.z;
		currentPosition = transform.position;
		rotateSpeed = Random.Range (-30, 30);
		x = (float)Random.Range (-6, 6)/10;
		y = (float)Random.Range (-5, 6)/10;
		z = (float)Random.Range (-6, 6)/10;
		Color myColor = GetComponent<Renderer> ().material.color;
		int a = Random.Range (2, 20);
		float c = (float)a / 100;
//		Debug.Log (c);
		rend.material.color = new Color (myColor.r, myColor.g, myColor.b, c);
	}
	
	// Update is called once per frame
	public void UpdateStep () {

		transform.Rotate (rotateSpeed*Time.deltaTime,rotateSpeed*Time.deltaTime,rotateSpeed*Time.deltaTime);

//		transform.Translate (x*Time.deltaTime, y*Time.deltaTime, 0);
		transform.position += new Vector3 (x * Time.deltaTime, y * Time.deltaTime,z*Time.deltaTime);

	
		if (transform.position.x < -7) {
			x *= -1;
			transform.position = new Vector3 (-6.9f, transform.position.y, transform.position.z);
		} else if (transform.position.x > 7) {
			x *= -1;
			transform.position = new Vector3 (6.9f, transform.position.y, transform.position.z);
		} else 	if (transform.position.y > 10) {
			y *= -1;
			transform.position = new Vector3 (transform.position.x, 9.9f, transform.position.z);
		}else 	if (transform.position.y < -5) {
			y *= -1;
			transform.position = new Vector3 (transform.position.x, -4.9f,transform.position.z);
		}

		if (transform.position.z > currentZ + 5) {
			z *= -1;
			transform.position = new Vector3 (transform.position.x, transform.position.y, currentZ + 4.9f);

		} else if (transform.position.z < currentZ - 5) {
			z *= -1;
			transform.position = new Vector3 (transform.position.x, transform.position.y, currentZ - 4.9f);
		}
	}
}
