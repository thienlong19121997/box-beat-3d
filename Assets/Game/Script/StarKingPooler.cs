﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarKingPooler : MonoBehaviour {


	public static StarKingPooler instance;

	public GameObject prefabs;
	public List<GameObject> listObject = new List<GameObject>();

	public GameObject prefabs2;
	public List<GameObject> listObject2 = new List<GameObject>();

	int id;
	// Use this for initialization
	void Awake () {
		instance = this;
	}

	// Update is called once per frame
	void Start () {
		SpawnObject ();
	}

	void SpawnObject(){
		for(int i = 0 ; i < 15 ; i++){
			GameObject objectClone = Instantiate (prefabs) as GameObject;
			objectClone.transform.SetParent (transform);
			objectClone.SetActive (false);
			listObject.Add (objectClone);
			id++;
			GameObject object2Clone = Instantiate (prefabs2) as GameObject;
			object2Clone.transform.SetParent (transform);
			object2Clone.SetActive (false);
			listObject2.Add (object2Clone);
			id++;
		}
	}

	public GameObject GetObjectStar(){
		for (int i = 0; i < listObject.Count; i++) {
			if (listObject [i].activeInHierarchy == false) {

				listObject [i].SetActive (true);
				return listObject [i];
			}
		}

		GameObject objectClone = Instantiate (prefabs) as GameObject;
		objectClone.transform.SetParent (transform);
		objectClone.SetActive (true);

		listObject.Add (objectClone);

		return objectClone;
	}

	public GameObject GetObjectKing(){
		for (int i = 0; i < listObject2.Count; i++) {
			if (listObject2 [i].activeInHierarchy == false) {
				listObject2 [i].SetActive(true);

				return listObject2 [i];
			}
		}

		GameObject object2Clone = Instantiate (prefabs2) as GameObject;
		object2Clone.transform.SetParent (transform);
		object2Clone.SetActive(true);

		listObject2.Add (object2Clone);

		return object2Clone;
	}



}
