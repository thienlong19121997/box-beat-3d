﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Particle : MonoBehaviour {

	void OnEnable(){
		Invoke ("Recycle",1.5f);
	}

	void Recycle(){
		gameObject.SetActive (false);
	}
}
