﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectMusic : MonoBehaviour {

	public static EffectMusic instance;

	public AudioClip button,close,harp,levelup,livepop,lose,musical,coinpop;
	AudioSource audioSource;


	void Awake(){
		instance = this;
		audioSource = GetComponent<AudioSource> ();
	}

	public void PlayOneShot(AudioClip clip){
		audioSource.PlayOneShot (clip);
	}




}
