﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SonicBloom.Koreo;
using SonicBloom.Koreo.Players;
using UnityEngine.UI;

public class Koreo : MonoBehaviour {

	public static Koreo instance;

	private Koreography koreography;
	private KoreographyTrackBase trackBase;
	private SimpleMusicPlayer smp;
	private List<KoreographyEvent> eventList = new List<KoreographyEvent>();

	public AudioSource audioSource;

	public Koreography koreo_event;
	public List<Koreography> songsList_classic = new List<Koreography>();
	public List<Koreography> songsList_modern = new List<Koreography>();
	public List<Koreography> songsList_orchestra = new List<Koreography>();
	public List<Koreography> songsList_edm = new List<Koreography>();

	public string namesong;
	public int numSong;
	private const float sampleRate = 44100;
	public float sumEvent;
	public float sample;
	private int evt;
	public float time;
	int lane;
	int maxLane;
	int ml,mr;
	bool ismr;
	bool type2;
	int typeNote;
	int lanen;
	public float level;
	public string mode;
	bool isRegister;
	public Text speed;
	int seven = 7;
	bool changeLane;
	public bool isModeKing;
	bool isFlash;
	// event here
	public bool isGiftEvent;
	public GameObject flashPanel;
	public GameObject EVENT;
	public Animator giftImage;
	public Text giftText;
	public int numGift;

	public ClassicPanel classicPanel;
	public ModernPanel modernPanel;
	public OrchestraPanel orchestraPanel;
	public EDMPanel edmPanel;
	public Text nameSongText;
	public Text nameSongText2;
	public AudioSource audioSource_lastsong;


	void Awake(){
		instance = this;
		smp = GetComponent<SimpleMusicPlayer> ();
		audioSource = GetComponent<AudioSource> ();
	}

	void Start () {
		maxLane = Manager.instance.lanes.Length;

		Invoke ("SetLastSong", .1f);
		Invoke("SetSong",0.2f);

	}

	public void SetLastSong(){
		numSong = PlayerPrefs.GetInt ("lastsong");
		namesong = PlayerPrefs.GetString ("namelastsong");
		mode = PlayerPrefs.GetString ("modelastsong");
		nameSongText.text = namesong;
		nameSongText2.text = namesong;
		if (mode == "classic") {
			audioSource_lastsong.clip = BackgroundMusic.instance.songsClip_classic [numSong];
		} else if (mode == "modern") {
			audioSource_lastsong.clip = BackgroundMusic.instance.songsClip_modern [numSong];
		} else if (mode == "orchestra") {
			audioSource_lastsong.clip = BackgroundMusic.instance.songsClip_orchestra [numSong];
		} else if (mode == "edm") {
			audioSource_lastsong.clip = BackgroundMusic.instance.songsClip_edm [numSong];
		}
	}

	void SetSong(){
		if (mode == "classic") {
			audioSource_lastsong.clip = BackgroundMusic.instance.songsClip_classic [numSong];
		} else if (mode == "modern") {
			audioSource_lastsong.clip = BackgroundMusic.instance.songsClip_modern [numSong];
		} else if (mode == "orchestra") {
			audioSource_lastsong.clip = BackgroundMusic.instance.songsClip_orchestra [numSong];
		} else if (mode == "edm") {
			audioSource_lastsong.clip = BackgroundMusic.instance.songsClip_edm [numSong];
		}
		audioSource_lastsong.Play ();
	}
		
	public void PlayGame(int num,bool isNextLevel ){
		Refresh (isNextLevel);
		if(num != -1)
		numSong = num;
		audioSource.Stop ();
		speed.text = "speed x"   + 	audioSource.pitch.ToString ();
		if (num == -1) {
			smp.LoadSong (koreo_event, 0, false);
		} else {
			if (mode == "classic") {
				smp.LoadSong (songsList_classic [numSong], 0, false);
				namesong = classicPanel.nameSongs_Classic [numSong];
			} else if (mode == "modern") {
				smp.LoadSong (songsList_modern [numSong], 0, false);
				namesong = modernPanel.nameSongs_modern [numSong];
			} else if (mode == "orchestra") {
				smp.LoadSong (songsList_orchestra [numSong], 0, false);
				namesong = orchestraPanel.nameSongs_orchestra [numSong];
			} else if (mode == "edm") {
				smp.LoadSong (songsList_edm [numSong], 0, false);
				namesong = edmPanel.nameSongs_edm [numSong];
			} else {
				return;
			}
		}
		PlayerPrefs.SetInt ("lastsong", numSong);
		PlayerPrefs.SetString ("modelastsong", mode);
		PlayerPrefs.SetString("namelastsong",namesong);
		koreography = Koreographer.Instance.GetKoreographyAtIndex (0);
		trackBase = koreography.GetTrackByID ("Note");
		eventList = trackBase.GetAllEvents ();
		sumEvent = (float)(eventList.Count - 1);
//		smp.Play ();
//		audioSource.Play ();
		timeToPlay = timeDelay;
		Manager.instance.stateGame = Manager.StateGame.playing;
		Manager.instance.isCanTouch = true;
		if (isRegister == false) {
			Koreographer.Instance.RegisterForEvents ("Note", EventHere);
			Koreographer.Instance.RegisterForEvents ("Tempo", TemposCurve);

			isRegister = true;
		}
			
		if (isFlash == false) {
			isFlash = true;
			flashPanel.SetActive (false);
			flashPanel.SetActive (true);
		}
//		koreography.get
	}

	public float valueCurve;

	void TemposCurve(KoreographyEvent evt){
		valueCurve = evt.GetValueOfCurveAtTime ((int)sample);

	}

	public void EventHere(KoreographyEvent evt){
		if (evt.StartSample == eventList [eventList.Count - 1].StartSample) {
			if (isGiftEvent == false && level == 0) {
				isGiftEvent = true;

				Invoke ("ChangeModeEventGift", 1.5f);

			} else {
				if (isGiftEvent) {
					StartCoroutine (HideEVENT ());
					isGiftEvent = false;
				}

				Invoke ("ChangeModeEndLess", 1.5f);

			}
		}
	}

	void ChangeModeEndLess(){
		Invoke ("DelayChangeStarKing", 1.5f);
		Debug.Log (" complete song ");
		level += 1;
		PlayGame (numSong, true);
	}

	void ChangeModeEventGift(){
		if (Manager.instance.isGameover)
			return;
		isGiftEvent = true;
		numGift = 0;
		giftText.text = "";
		EVENT.SetActive (true);
		giftImage.SetTrigger ("active");

		giftText.gameObject.SetActive (true);

		PlayGame (-1, true);
	}

	IEnumerator HideEVENT(){
		yield return new WaitForSeconds (0.4f);
		giftImage.SetTrigger ("hide");
		giftText.gameObject.SetActive (false);
	}

	void DelayChangeStarKing(){
		if (isModeKing == false) {
			isGiftEvent = false;

			InGame.instance.Mode_King ();
			isModeKing = true;
		}
	}

	float timeDelay = 2f;
	float timeToPlay;

	public void UpdateStep(){
		if (Manager.instance.stateGame != Manager.StateGame.playing)
			return;

		if (timeToPlay > 0 && Manager.instance.isPaused == false) {
			timeToPlay -= Time.deltaTime;
			if (timeToPlay < 0) {
				smp.Play ();
				audioSource.Play ();
			}
		}
		sample = (float)koreography.GetLatestSampleTime () - timeToPlay*44100;
		time = (float)sample / 44100;
		Debug.Log (timeToPlay);
		int cout = eventList.Count - 1;
		for (int i = evt; i < cout; i++) {
			if (sample >= eventList [i].StartSample - 44100 * (4)) {
				GameObject noteClone = NotePooler.instance.GetObject ();
				Note2 note2 = noteClone.GetComponent<Note2> ();
				string textValue = eventList [i].GetTextValue ();


				if (textValue != "") {
					if (int.Parse (textValue) == 7) {
						if (changeLane == false) {
							changeLane = true;
							seven = 7;
							typeNote = Random.Range (0, 3);
							//						lane = int.Parse (textValue) - 1;
							RandomLane ();
							if (lane == maxLane - 1) {
								ismr = true;
							} else if (lane == 0) {
								ismr = false;
							}
						} else {
							lane = Random.Range (0, maxLane);
							lanen = lane;
							seven = 7;
						}

					} else if (int.Parse (textValue) == 8) {
//						lane = Random.Range (0, maxLane);
//						seven = 8;
					} else if (int.Parse (textValue) == 1) {
						changeLane = false;
						seven = 0;
						typeNote = Random.Range (0, 3);
//						lane = int.Parse (textValue) - 1;
						RandomLane ();
						if (lane == maxLane - 1) {
							ismr = true;
						} else if (lane == 0) {
							ismr = false;
						}
					} else {
						lane = int.Parse (textValue);
						seven = 8;
					}
					mr = ml = 0;
				} else {
					if (seven == 8) {
						lane = int.Parse (textValue);
						seven = 8;
					} else if (seven == 7) {
						
						RandomLane ();

					} else {

						if (typeNote == 0) {
							if (ismr == false) {
								type2 = !type2;
								if (type2)
									lane++;
							} else {
								type2 = !type2;
								if (type2)
									lane--;
							}
							if (lane == maxLane - 1) {
								ismr = true;
							} else if (lane == 0) {
								ismr = false;
							}
						} else if (typeNote == 1) {
							if (ismr == false) {
								lane++;
							} else {
								lane--;
							}
							if (lane == maxLane - 1) {
								ismr = true;
							} else if (lane == 0) {
								ismr = false;
							}
						} else if (typeNote == 2) {
							if (ismr == false) {
								ml++;
								if (ml <= 3) {
									lane++;
								} else if (ml <= 5) {
									lane--;
								} else if (ml <= 8) {
									lane++;
								} else if (ml <= 10) {
									lane--;
								} else if (ml <= 13) {
									lane++;
								} else if (ml <= 15) {
									lane--;
								} else if (ml <= 18) {
									lane++;
								} else if (ml <= 20) {
									lane--;
								} else if (ml <= 23) {
									lane++;
								} else if (ml <= 25) {
									lane--;
								} else if (ml <= 28) {
									lane++;
								} else if (ml <= 30) {
									lane--;
								} else {
									lane++;
								}
								if (lane == maxLane - 1) {
									ismr = true;
									ml = 0;
								}

							} else {
								mr++;
								if (mr <= 3) {
									lane--;
								} else if (mr <= 5) {
									lane++;
								} else if (mr <= 8) {
									lane--;
								} else if (mr <= 10) {
									lane++;
								} else if (mr <= 13) {
									lane--;
								} else if (mr <= 15) {
									lane++;
								} else if (mr <= 18) {
									lane--;
								} else if (mr <= 20) {
									lane++;
								} else if (mr <= 23) {
									lane--;
								} else if (mr <= 25) {
									lane++;
								} else if (mr <= 28) {
									lane--;
								} else if (mr <= 30) {
									lane++;
								} else {
									lane--;
								}
								if (lane == 0) {
									ismr = false;
									mr = 0;
								}
							}
						}
					}
				}
				lane = Mathf.Clamp (lane, 0, 8);

				note2.OnNote (sample, eventList [i].StartSample, lane);

				evt++;
			}
		}
		
	}

	void Refresh(bool nextLevel){
		maxLane = Manager.instance.lanes.Length;
		mr = ml  = lane  =0;
		sample = 0;

		time = 0;
		evt = 0;
		NotePooler.instance.Refresh ();
		ParticalPooler.instance.Refresh ();
		if (nextLevel == false) {
			Manager.instance.Refresh ();
			level = 0;
			isGiftEvent = false;
			isModeKing = false;
			isFlash = false;
			InGame.instance.Mode_Star ();
		}
	
		audioSource.pitch = 1 + (level/10);
		sumEvent = 0;
	}

	void RandomLane(){
		if (lanen <= 0) {
			lane = Random.Range (1, 4);
		} else if (lanen == 1) {
			lane = Random.Range (2, 5);
		} else if (lanen == 2) {
			lane = Random.Range (0, 6);
		} else if (lanen == 3) {
			lane = Random.Range (0, 7);
		} else if (lanen == 4) {
			lane = Random.Range (0, 9);
		} else if (lanen == 5) {
			lane = Random.Range (2, 9);
		}
		else if (lanen == 6) {
			lane = Random.Range (3, 9);
		} else if (lanen == 7) {
			lane = Random.Range (4, 9);
		} else if (lanen == 8) {
			lane = Random.Range (5, 8);
		} else {
			lane = Random.Range (0, 9);
		}
		lane = Mathf.Clamp (lane, 0, 8);
		lanen = lane;
	}
}
