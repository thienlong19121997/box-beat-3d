﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

	public static UIManager instance;

	[Header("Menu")]
	public GameObject mainMenu;
	public GameObject ingameMenu;
	public GameObject gameoverMenu;
	public GameObject homeMenu;
	public GameObject songMenu;
	public GameObject storeMenu;
	public GameObject infoMenu;
	public GameObject iapMenu;
	public GameObject revivePopup;
	public GameObject pauseMenu;
	public GameObject btnGame;
	public GameObject canvas_overlay;
	public GameObject level;
	public GameObject[] btnPlusIAP;
	public GameObject panel_gem;
	public GameObject panel_life;
	public GameObject panel_gift;
	public GameObject panel_theme;
	public GameObject panel_premium;
	public GameObject panel_gem_theme_line;
	public GameObject levelup;
	public Text btn_Gem_text;
	public Text btn_Life_text;
	public Text btn_Gift_text;
	public Text btn_Theme_text;
	public Text btn_Premium_text;
	public Color colorActive;
	public GameObject cant;
	public GameObject can;

	public int gemForRevive;
	public Text gemForReviveText;
	[Header("Text")]
	public Text scoreText_ingame;
	public Text gemText;
	public Text missText;
	public Text liveText;
	public Text giftText;
	public Text plusLifeText;
	public Text gemPlusText;
	public Text giftPlusText;

	[Header("Animator")]
	public Animator plusLifeAnim;
	public Animator gemPlusAnim;
	public Animator giftPlusAnim;
	[Header("Sprite")]
	public Sprite[] sprMain_UnActive;
	public Sprite[] sprMain_Active;
	public Image[] btnMain;

	public GameOver gameoverScript;

	[Header("ListSong")]
	public GameObject[] listsong_panel;
	public Text[] listsong_text;

	public GameObject lineListSong;

	public enum StateMain{
		home,
		song,
		store,
		info
	}
	public StateMain stateMain;

	public GameObject btnPause;

	void Awake(){
		instance = this;
	}
	void Start(){
		UpdateUI ();

	}

	public void BtnPlayGame(int numSong){
		if (PlayerPrefs.GetInt ("live") <= 0) {
			BtnExit ();
			BtnLIFE ();
			return;
		}

		Manager.instance.MinusLive (1);
		canvas_overlay.SetActive (true);
		ingameMenu.SetActive (true);
		level.SetActive (false);
		for (int i = 0; i < btnPlusIAP.Length; i++) {
			btnPlusIAP [i].SetActive (false);
		}
		mainMenu.SetActive (false);
		gameoverMenu.SetActive (false);
		homeMenu.SetActive (false);
		songMenu.SetActive (false);
		storeMenu.SetActive (false);
		infoMenu.SetActive (false);
		revivePopup.SetActive (false);
		pauseMenu.SetActive (false);
		Manager.instance.isPaused = false;
		btnPause.SetActive (true);
		levelup.SetActive (false);

		Koreo.instance.PlayGame (numSong,false);
		BackgroundMusic.instance.StopMusic ();
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.button);
		BackgroundMusic.instance.PauseLastSongMusic ();

	}

	public void PlayGameInHomeMenu(){
		if (PlayerPrefs.GetInt ("live") <= 0) {
			BtnExit ();
			BtnLIFE ();
			return;
		}

		Manager.instance.MinusLive (1);
		canvas_overlay.SetActive (true);
		ingameMenu.SetActive (true);
		level.SetActive (false);
		for (int i = 0; i < btnPlusIAP.Length; i++) {
			btnPlusIAP [i].SetActive (false);
		}
		mainMenu.SetActive (false);
		gameoverMenu.SetActive (false);
		homeMenu.SetActive (false);
		songMenu.SetActive (false);
		storeMenu.SetActive (false);
		infoMenu.SetActive (false);
		revivePopup.SetActive (false);
		pauseMenu.SetActive (false);
		Manager.instance.isPaused = false;

		btnPause.SetActive (true);
		levelup.SetActive (false);

		Koreo.instance.PlayGame (PlayerPrefs.GetInt("lastsong"),false);
		BackgroundMusic.instance.StopMusic ();
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.button);
		BackgroundMusic.instance.PauseLastSongMusic ();

	}

	public void GameOver(){
		StartCoroutine (DelayActiveGameover ());
	}

	IEnumerator DelayActiveGameover()
	{
		yield return new WaitForSeconds (2);
		mainMenu.SetActive (false);
		canvas_overlay.SetActive (false);
		level.SetActive (false);
		for (int i = 0; i < btnPlusIAP.Length; i++) {
			btnPlusIAP [i].SetActive (false);
		}
		ingameMenu.SetActive (false);
		gameoverMenu.SetActive (true);
	}

	public void BtnExit(){
		levelup.SetActive (false);
		revivePopup.SetActive (false);
		pauseMenu.SetActive (false);
		Manager.instance.isPaused = false;

		canvas_overlay.SetActive (false);
		level.SetActive (true);
		for (int i = 0; i < btnPlusIAP.Length; i++) {
			btnPlusIAP [i].SetActive (true);
		}
		ingameMenu.SetActive (false);
		gameoverMenu.SetActive (false);
		mainMenu.SetActive (true);
		BtnHome ();
		UpdateUI ();
		BackgroundMusic.instance.StopMusic ();
		homeMenu.SetActive (true);
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.button);
		BackgroundMusic.instance.PlayMusicLastSong ();

	}

	public void BtnRetry(){
		BtnPlayGame (Koreo.instance.numSong);
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.button);
	}

	public void Revive(){
		revivePopup.SetActive (true);
		if (Koreo.instance.isModeKing == false) {
			if (PlayerPrefs.GetFloat ("gem") >= (200 + gemForRevive)) {
				can.SetActive (true);
				cant.SetActive (false);
			} else {
				can.SetActive (false);
				cant.SetActive (true);
			}
		} else {
			if (PlayerPrefs.GetFloat ("gem") >= (2000 + gemForRevive)) {
				can.SetActive (true);
				cant.SetActive (false);
			} else {
				can.SetActive (false);
				cant.SetActive (true);
			}
		}
	}


	public void BtnRevive(){
		if (PlayerPrefs.GetFloat ("gem") < gemForRevive) {
			return;
		}
		StartCoroutine (DelayAudio (1.5f));
		revivePopup.SetActive (false);
		Manager.instance.MinusGem (gemForRevive);
		Manager.instance.isCanTouch = true;
		Manager.instance.miss = 1;
		Manager.instance.isGameover = false;
//		Manager.instance.rate.transform.localScale = new Vector3 (1f, 1f, 1f);
		Invoke("DelayActivePause",2);
		gemForRevive *= 2;
		gemForReviveText.text = gemForRevive.ToString ();
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.button);
	}

	void DelayActivePause(){
		btnPause.SetActive(true);

	}

	IEnumerator DelayAudio(float time){
		yield return new WaitForSeconds (time);
		Koreo.instance.audioSource.Play ();
	}

	public void BtnReviveAds(){
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.button);
	}

	public void BtnPause(){
		if (Koreo.instance.isGiftEvent)
			return;
		Manager.instance.isCanTouch = false;
		pauseMenu.SetActive (true);
		Koreo.instance.audioSource.Pause ();
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.button);
		Manager.instance.isPaused = true;
	}

	public void Resume(){
		Manager.instance.isCanTouch = true;

		pauseMenu.SetActive (false);
		Manager.instance.isPaused = false;
		StartCoroutine (DelayAudio (1));
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.button);

	}

	public void BtnExitPopup(){
		Manager.instance.GameOver ();
		revivePopup.SetActive (false);
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.button);
	}

	void ActiveButton(int num){
		for (int i = 0; i < 4; i++) {
			if (i == num) {
				btnMain [i].sprite = sprMain_Active [i];
			} else {
				btnMain [i].sprite = sprMain_UnActive [i];
			}
		}
	}

	public void BtnHome(){
		if (stateMain == StateMain.home) {
			return;
		}
		btnGame.SetActive (false);
		btnGame.SetActive (true);
		stateMain = StateMain.home;
		RewardManager.instance.UpdateReward ();
		homeMenu.SetActive (true);
		songMenu.SetActive (false);
		storeMenu.SetActive (false);
		infoMenu.SetActive (false);
		iapMenu.SetActive (false);
		ActiveButton (0);
		Koreo.instance.SetLastSong ();
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.button);
	}

	public void BtnSong(){
		if (stateMain == StateMain.song) {
			return;
		}
		btnGame.SetActive (false);
		btnGame.SetActive (true);
		stateMain = StateMain.song;
		homeMenu.SetActive (false);
		songMenu.SetActive (true);
		storeMenu.SetActive (false);
		infoMenu.SetActive (false);
		iapMenu.SetActive (false);
		ActiveButton (1);
		BtnListSongint (0);
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.button);
	}

	public void BtnStore(){
		if (stateMain == StateMain.store) {
			return;
		}
		btnGame.SetActive (false);
		btnGame.SetActive (true);
		stateMain = StateMain.store;
		homeMenu.SetActive (false);
		songMenu.SetActive (false);
		storeMenu.SetActive (true);
		infoMenu.SetActive (false);
		iapMenu.SetActive (false);
		BtnPanelGem ();
		ActiveButton (2);
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.button);
	}

	public void BtnInfo(){
		if (stateMain == StateMain.info) {
			return;
		}
		btnGame.SetActive (false);
		btnGame.SetActive (true);
		stateMain = StateMain.info;
		homeMenu.SetActive (false);
		songMenu.SetActive (false);
		storeMenu.SetActive (false);
		infoMenu.SetActive (true);
		iapMenu.SetActive (false);
		ActiveButton (3);
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.button);
	}

	public void BtnIAP(){
		BtnStore ();
		BtnPanelGem ();
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.button);
	}

	public void BtnToGift(){
		BtnStore ();
		BtnPanelGift ();
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.button);
	}

	public void BtnLIFE(){
		BtnStore ();
		BtnPanelLife ();
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.button);
	}

	public void BtnPanelGem(){
		panel_gem.SetActive (true);
		panel_gem_theme_line.transform.position = new Vector3 (btn_Gem_text.gameObject.transform.position.x, panel_gem_theme_line.transform.position.y, panel_gem_theme_line.transform.position.z);
		btn_Gem_text.color = colorActive;

		panel_theme.SetActive (false);
		panel_life.SetActive (false);
		panel_gift.SetActive (false);
		panel_premium.SetActive (false);
		btn_Theme_text.color = new Color (1, 1, 1, 1);
		btn_Life_text.color  = new Color (1, 1, 1, 1);
		btn_Gift_text.color = new Color (1, 1, 1, 1);
		btn_Premium_text.color  = new Color (1, 1, 1, 1);
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.button);
	}

	public void BtnPanelLife(){
		panel_life.SetActive (true);
		panel_gem_theme_line.transform.position = new Vector3 (btn_Life_text.gameObject.transform.position.x, panel_gem_theme_line.transform.position.y, panel_gem_theme_line.transform.position.z);
		btn_Life_text.color = colorActive;

		panel_theme.SetActive (false);
		panel_gem.SetActive (false);
		panel_gift.SetActive (false);
		panel_premium.SetActive (false);
		btn_Theme_text.color = new Color (1, 1, 1, 1);
		btn_Gem_text.color = new Color (1, 1, 1, 1);
		btn_Gift_text.color = new Color (1, 1, 1, 1);
		btn_Premium_text.color  = new Color (1, 1, 1, 1);
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.button);
	}

	public void BtnPanelTheme(){
		panel_theme.SetActive (true);
		panel_gem_theme_line.transform.position = new Vector3 (btn_Theme_text.gameObject.transform.position.x, panel_gem_theme_line.transform.position.y, panel_gem_theme_line.transform.position.z);
		btn_Theme_text.color = colorActive;

		panel_gift.SetActive (false);
		panel_premium.SetActive (false);
		panel_gem.SetActive (false);
		panel_life.SetActive (false);
		btn_Gem_text.color = new Color (1, 1, 1, 1);
		btn_Life_text.color = new Color (1, 1, 1, 1);
		btn_Gift_text.color = new Color (1, 1, 1, 1);
		btn_Premium_text.color  = new Color (1, 1, 1, 1);
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.button);
	}

	public void BtnPanelGift(){
		panel_gift.SetActive (true);
		panel_gem_theme_line.transform.position = new Vector3 (btn_Gift_text.gameObject.transform.position.x, panel_gem_theme_line.transform.position.y, panel_gem_theme_line.transform.position.z);
		btn_Gift_text.color = colorActive;

		panel_gem.SetActive (false);
		panel_premium.SetActive (false);
		panel_theme.SetActive (false);
		panel_life.SetActive (false);
		btn_Gem_text.color = new Color (1, 1, 1, 1);
		btn_Life_text.color = new Color (1, 1, 1, 1);
		btn_Theme_text.color = new Color (1, 1, 1, 1);
		btn_Premium_text.color  = new Color (1, 1, 1, 1);
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.button);
	}

	public void BtnPanelPremium(){
		panel_premium.SetActive (true);
		panel_gem_theme_line.transform.position = new Vector3 (btn_Premium_text.gameObject.transform.position.x, panel_gem_theme_line.transform.position.y, panel_gem_theme_line.transform.position.z);
		btn_Premium_text.color = colorActive;

		panel_gem.SetActive (false);
		panel_gift.SetActive (false);
		panel_theme.SetActive (false);
		panel_life.SetActive (false);
		btn_Gem_text.color = new Color (1, 1, 1, 1);
		btn_Life_text.color = new Color (1, 1, 1, 1);
		btn_Theme_text.color = new Color (1, 1, 1, 1);
		btn_Gift_text.color  = new Color (1, 1, 1, 1);
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.button);
	}

	public void BtnListSongint(int num){
		for(int i = 0 ; i < 4 ; i++){
			if (num == i) {
				listsong_panel [i].SetActive (true);
				listsong_text [i].color = colorActive;
				lineListSong.transform.position = new Vector3 (listsong_text [i].gameObject.transform.position.x, lineListSong.transform.position.y, lineListSong.transform.position.z);
			} else {
				listsong_panel [i].SetActive (false);
				listsong_text [i].color = new Color (1, 1, 1, 1);
			}

			if (num == 0) {
				Koreo.instance.mode = "classic";
			} else if (num == 1) {
				Koreo.instance.mode = "modern";
			} else if (num == 2) {
				Koreo.instance.mode = "orchestra";
			} else if (num == 3) {
				Koreo.instance.mode = "edm";
			}
		}
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.button);

	}

	public void BuyLife(int num){
		if (num == 1) {
			if (PlayerPrefs.GetInt ("gift") >= 50) {
				Manager.instance.MinusGift (50);
				Manager.instance.PlusLive (1);
				EffectMusic.instance.PlayOneShot (EffectMusic.instance.livepop);
			} else {
				BtnPanelGift ();
			}
		} else if (num == 2) {
			if (PlayerPrefs.GetInt ("gift") >= 95) {
				Manager.instance.MinusGift (95);
				Manager.instance.PlusLive (2);
				EffectMusic.instance.PlayOneShot (EffectMusic.instance.livepop);
			} else {
				BtnToGift ();
			}
		} else if (num == 3) {
			if (PlayerPrefs.GetInt ("gift") >= 140) {
				Manager.instance.MinusGift (140);
				Manager.instance.PlusLive (3);
				EffectMusic.instance.PlayOneShot (EffectMusic.instance.livepop);
			} else {
				BtnToGift ();
			}
		} else if (num == 4) {
			if (PlayerPrefs.GetInt ("gift") >= 180) {
				Manager.instance.MinusGift (180);
				Manager.instance.PlusLive (4);
				EffectMusic.instance.PlayOneShot (EffectMusic.instance.livepop);
			} else {
				BtnToGift ();
			}
		}

	}

	public void BuyGift(int num){
		if (num == 50) {
			if (PlayerPrefs.GetFloat ("gem") >= 200) {
				Manager.instance.MinusGem (200);
				Manager.instance.PlusGift (50);
				EffectMusic.instance.PlayOneShot (EffectMusic.instance.livepop);
			} else {
				BtnPanelGift ();
			}
		} else if (num == 100) {
			if (PlayerPrefs.GetFloat ("gem") >= 390) {
				Manager.instance.MinusGem (390);
				Manager.instance.PlusGift (100);
				EffectMusic.instance.PlayOneShot (EffectMusic.instance.livepop);
			} else {
				BtnToGift ();
			}
		} else if (num == 150) {
			if (PlayerPrefs.GetFloat ("gem") >= 580) {
				Manager.instance.MinusGem (580);
				Manager.instance.PlusGift (150);
				EffectMusic.instance.PlayOneShot (EffectMusic.instance.livepop);
			} else {
				BtnToGift ();
			}
		} else if (num == 250) {
			if (PlayerPrefs.GetFloat ("gem") >= 950) {
				Manager.instance.MinusGem (950);
				Manager.instance.PlusGift (250);
				EffectMusic.instance.PlayOneShot (EffectMusic.instance.livepop);
			} else {
				BtnIAP ();
			}
		}

	}


	public GameObject panelTest;
	public void BtnTest(){
		Manager.instance.isNeverDie = !Manager.instance.isNeverDie;
		panelTest.SetActive (Manager.instance.isNeverDie);
	}

	public void UpdateUI(){
		gameoverScript.UpdateLevelText2 ();
		RewardManager.instance.UpdateReward ();
		Manager.instance.PlusGem (0);
		Manager.instance.PlusGift (0);
		Manager.instance.PlusLive (0);

//		MenuSong.instance.UpdateMenuHome ();
	}

	public void BtnGetReward_NumTap(){
		
		int g_1000 = PlayerPrefs.GetInt ("Get_NumTap_1000");
		int g_5000 = PlayerPrefs.GetInt ("Get_NumTap_5000");
		int g_10k = PlayerPrefs.GetInt ("Get_NumTap_10k");
		int g_30k = PlayerPrefs.GetInt ("Get_NumTap_30k");
		int g_60k = PlayerPrefs.GetInt ("Get_NumTap_60k");
		int g_100k = PlayerPrefs.GetInt ("Get_NumTap_100k");

		StartCoroutine(SpawnGem(PlayerPrefs.GetInt("numtapgem")/10,RewardManager.instance.btnGet_numtap.transform.position));

		if (g_1000 == 0) {
			PlayerPrefs.SetInt ("Get_NumTap_1000", 1);
			PlayerPrefs.SetInt ("numtapgem", 300);
		} else if (g_5000 == 0) {
			PlayerPrefs.SetInt ("Get_NumTap_5000", 1);
			PlayerPrefs.SetInt ("numtapgem", 500);
		} else if (g_10k == 0) {
			PlayerPrefs.SetInt ("Get_NumTap_10k", 1);
			PlayerPrefs.SetInt ("numtapgem", 800);
		} else if (g_30k == 0) {
			PlayerPrefs.SetInt ("Get_NumTap_30k", 1);
			PlayerPrefs.SetInt ("numtapgem", 1100);
		} else if (g_60k == 0) {
			PlayerPrefs.SetInt ("Get_NumTap_60k", 1);
			PlayerPrefs.SetInt ("numtapgem", 1500);
		} else if (g_100k == 0) {
			PlayerPrefs.SetInt ("Get_NumTap_100k", 1);
			PlayerPrefs.SetInt ("numtapgem", 2000);
		} 

		RewardManager.instance.UpdateReward ();
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.livepop);
	}

	public void BtnGetReward_SumPerfect(){

		int g_500 = PlayerPrefs.GetInt  ("Get_SumPerfect_500");
		int g_1000 = PlayerPrefs.GetInt ("Get_SumPerfect_2000");
		int g_5000 = PlayerPrefs.GetInt ("Get_SumPerfect_5000");
		int g_10k = PlayerPrefs.GetInt  ("Get_SumPerfect_10k");
		int g_15k = PlayerPrefs.GetInt  ("Get_SumPerfect_15k");
		int g_20k = PlayerPrefs.GetInt  ("Get_SumPerfect_20k");

		StartCoroutine(SpawnGem(PlayerPrefs.GetInt("sumperfectgem")/10,RewardManager.instance.btnGet_SumPerfect.transform.position));

		if (g_500 == 0) {
			PlayerPrefs.SetInt ("Get_SumPerfect_500", 1);
			PlayerPrefs.SetInt ("sumperfectgem", 300);
		} else if (g_1000 == 0) {
			PlayerPrefs.SetInt ("Get_SumPerfect_2000", 1);
			PlayerPrefs.SetInt ("sumperfectgem", 500);
		} else if (g_5000 == 0) {
			PlayerPrefs.SetInt ("Get_SumPerfect_5000", 1);
			PlayerPrefs.SetInt ("sumperfectgem", 800);
		} else if (g_10k == 0) {
			PlayerPrefs.SetInt ("Get_SumPerfect_10k", 1);
			PlayerPrefs.SetInt ("sumperfectgem", 1100);
		} else if (g_15k == 0) {
			PlayerPrefs.SetInt ("Get_SumPerfect_15k", 1);
			PlayerPrefs.SetInt ("sumperfectgem", 1500);
		} else if (g_20k == 0) {
			PlayerPrefs.SetInt ("Get_SumPerfect_20k", 1);
			PlayerPrefs.SetInt ("sumperfectgem", 2000);
		} 

		RewardManager.instance.UpdateReward ();
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.livepop);
	}

	public void BtnGetReward_ComboX100(){

		StartCoroutine(SpawnGem((PlayerPrefs.GetInt("COMBOX100")*50)/10,RewardManager.instance.btnGet_combox100.transform.position));
		PlayerPrefs.SetInt ("COMBOX100", 0);
		RewardManager.instance.UpdateReward ();
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.livepop);
	}

	public void BtnGetReward_Collect3Star(){

		StartCoroutine(SpawnGem((PlayerPrefs.GetInt("COLLECT3STAR")/3*100)/10,RewardManager.instance.btnGet_collect3star.transform.position));
		PlayerPrefs.SetInt ("COLLECT3STAR", 0);
		RewardManager.instance.UpdateReward ();
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.livepop);
	}

	IEnumerator SpawnGem(int num,Vector3 pos){
		for (int i = 0; i < num; i++) {
			GameObject gemClone = GemPooler.instance.GetObject ();
			gemClone.GetComponent<Gem> ().currentPos = pos;
			yield return new WaitForSeconds (0.01f);
		}
	}

	public void BtnViewAds(){
		UnityAdsManager.Instance.ShowRewardAds ();
		UIManager.instance.gemPlusText.text = "+" + 50;
		UIManager.instance.gemPlusAnim.SetTrigger ("active");
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.button);
	}

	public void BtnHelixHoop(){
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.button);
		#if UNITY_IOS
		Application.OpenURL("itms-apps://itunes.apple.com/app/id1327168260");
		#endif
	}

	public void BtnKingBallIO(){
		EffectMusic.instance.PlayOneShot (EffectMusic.instance.button);
		#if UNITY_IOS
		Application.OpenURL("itms-apps://itunes.apple.com/app/id1395136315");
		#endif
	}
}
