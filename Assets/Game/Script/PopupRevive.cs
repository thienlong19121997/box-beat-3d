﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupRevive : MonoBehaviour {

	float time = 5;
	bool isBool;
	public Image fill;
	void OnEnable(){
		time = 5;
		isBool = false;
		fill.fillAmount = 1;
	}

	void Update(){
		if (time > 0) {
			time -= Time.deltaTime;
			fill.fillAmount = time/5;
		} else {
			if (isBool == false) {
				isBool = true;
				UIManager.instance.BtnExitPopup ();
			}
		}
	}
}
