﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RewardManager : MonoBehaviour {
	
	public static RewardManager instance;

	void Awake(){
		instance = this;
	}

	// number of tap
	[Header("Number Tap")]
	public Text numberTap_GemText;
	public GameObject numberTap_v1;
	public GameObject numberTap_v2;

	public Text[] targetNumTap_Text;
	public Image bar_fill_numtap;
	public Text[] targetNumTap_Text2;
	public Image bar_fill_numtap2;
	public GameObject btnGet_numtap;

	private const string NUMBER_OF_TAP   = "NUMBER_OF_TAP";

	public int Number_Of_Tap(){
		return PlayerPrefs.GetInt (NUMBER_OF_TAP);
	}

	public void Set_Number_Of_Tap(int num){
		int numberOfTab = PlayerPrefs.GetInt (NUMBER_OF_TAP);
		numberOfTab += num;
		PlayerPrefs.SetInt (NUMBER_OF_TAP, numberOfTab);
	}

	//

	// sumperfect
	[Header("Sum Perfect")]

	public Text sumPerfect_GemText;
	public GameObject sumPerfect_v1;
	public GameObject sumPerfect_v2;

	public Text[] targetSumPerfect_Text;
	public Image bar_fill_SumPerfect;
	public Text[] targetSumPerfect_Text2;
	public Image bar_fill_SumPerfect2;
	public GameObject btnGet_SumPerfect;

	private const string SUM_PERFECT   = "SUM_PERFECT";

	public int Sum_Of_Perfect(){
		return PlayerPrefs.GetInt (SUM_PERFECT);
	}

	public void Set_Sum_Of_Perfect(int num){
		int sumPerfect = PlayerPrefs.GetInt (SUM_PERFECT);
		sumPerfect += num;
		PlayerPrefs.SetInt (SUM_PERFECT, sumPerfect);
	}

	//
	[Header("combox100")]

	public Text combox100_text;
	public Image combox100_fill;
	public GameObject btnGet_combox100;

	private const string COMBOX100 = "COMBOX100";

	public int Combo_x_100(){
		return PlayerPrefs.GetInt (COMBOX100);
	}

	public void Set_Combo_x_100(int num){
		int c = PlayerPrefs.GetInt (COMBOX100);
		c += num;
		PlayerPrefs.SetInt (COMBOX100, c);
	}

	//
	[Header("collect3star")]
	public Text collect3star_text;
	public Image collect3star_fill;
	public GameObject btnGet_collect3star;

	private const string COLLECT3STAR = "COLLECT3STAR";

	public int Get_Collect_3_Star(){
		return PlayerPrefs.GetInt (COLLECT3STAR);
	}

	public void Set_Collect_3_Star(int num){
		int c = PlayerPrefs.GetInt (COLLECT3STAR);
		if (c < 3) {
			c += num;
			PlayerPrefs.SetInt (COLLECT3STAR, c);
		}
	}
	// Use this for initialization
	public void UpdateReward(){
		SumPerfect ();
		NumberTap ();
		ComboX100 ();
		Collect3Star ();
//		if (CheckInternetConnection ()) {
//			btnGet_ViewAds.SetActive (true);
//		} else {
//			btnGet_ViewAds.SetActive (false);
//		}
	}



	public void NumberTap(){
		int s = Number_Of_Tap ();

		numberTap_GemText.text = "RECEIVE "  + PlayerPrefs.GetInt ("numtapgem");

		if (PlayerPrefs.GetInt ("Get_NumTap_10k") == 0) {
			numberTap_v1.SetActive (true);
			numberTap_v2.SetActive (false);
			int get_1000 = PlayerPrefs.GetInt ("Get_NumTap_1000");
			int get_5000 = PlayerPrefs.GetInt ("Get_NumTap_5000");
			int get_10k = PlayerPrefs.GetInt ("Get_NumTap_10k");

			for (int i = 0; i < targetNumTap_Text.Length; i++) {
				targetNumTap_Text [i].color = new Color (targetNumTap_Text [i].color.r, targetNumTap_Text [i].color.g, targetNumTap_Text [i].color.b, .5f);				
			}

			btnGet_numtap.SetActive (false);
			if (s >= 1000 && get_1000 == 0) {
				btnGet_numtap.SetActive (true);
				targetNumTap_Text [0].color =  new Color (targetNumTap_Text [0].color.r, targetNumTap_Text [0].color.g, targetNumTap_Text [0].color.b, 1);	
			} 
			if (s >= 5000 && get_5000 == 0) {
				btnGet_numtap.SetActive (true);
				targetNumTap_Text [1].color = new Color (targetNumTap_Text [1].color.r, targetNumTap_Text [1].color.g, targetNumTap_Text [1].color.b, 1);	
			} 
			if (s >= 10000 && get_10k == 0) {
				btnGet_numtap.SetActive (true);
				targetNumTap_Text [2].color = new Color (targetNumTap_Text [2].color.r, targetNumTap_Text [2].color.g, targetNumTap_Text [2].color.b, 1);	
			}


			if (s <= 1000) {
				bar_fill_numtap.fillAmount = (float)s / 1000 * 0.333f;
			} else if (s <= 5000) {
				bar_fill_numtap.fillAmount = 0.333f + ((float)((s - 1000)) / (5000 - 1000)) * 0.333f;

			} else {
				bar_fill_numtap.fillAmount = 0.666f + ((float)((s - 5000)) / (10000 - 5000)) * 0.333f;
			}
		} else {
			numberTap_v1.SetActive (false);
			numberTap_v2.SetActive (true);

			int get_30k = PlayerPrefs.GetInt ("Get_NumTap_30k");
			int get_60k = PlayerPrefs.GetInt ("Get_NumTap_60k");
			int get_100k = PlayerPrefs.GetInt ("Get_NumTap_100k");

			for (int i = 0; i < targetNumTap_Text2.Length; i++) {
				targetNumTap_Text2 [i].color = new Color (targetNumTap_Text2 [i].color.r, targetNumTap_Text2 [i].color.g, targetNumTap_Text2 [i].color.b, .5f);			
			}

			btnGet_numtap.SetActive (false);

			if (s >= 30000 && get_30k == 0) {
				btnGet_numtap.SetActive (true);
				targetNumTap_Text2 [0].color = new Color (targetNumTap_Text2 [0].color.r, targetNumTap_Text2 [0].color.g, targetNumTap_Text2 [0].color.b, 1);	
			} 
			if (s >= 60000 && get_60k == 0) {
				btnGet_numtap.SetActive (true);
				targetNumTap_Text2 [1].color = new Color (targetNumTap_Text2 [1].color.r, targetNumTap_Text2 [1].color.g, targetNumTap_Text2 [1].color.b, 1);	
			} 
			if (s >= 100000 && get_100k == 0) {
				btnGet_numtap.SetActive (true);
				targetNumTap_Text2 [2].color = new Color (targetNumTap_Text2 [2].color.r, targetNumTap_Text2 [2].color.g, targetNumTap_Text2 [2].color.b, 1);	
			} 

			if (s <= 30000) {
				bar_fill_numtap2.fillAmount = (float)(s-10000) / (30000-10000) * 0.333f;
			} else if (s <= 60000) {
				bar_fill_numtap2.fillAmount = 0.333f + ((float)((s - 30000)) / (60000 - 30000)) * 0.333f;

			} else {
				bar_fill_numtap2.fillAmount = 0.666f + ((float)((s - 60000)) / (100000 - 60000)) * 0.333f;
			}
		}
	}

	public void SumPerfect(){
		int s = Sum_Of_Perfect ();

		sumPerfect_GemText.text = "RECEIVE "  + PlayerPrefs.GetInt ("sumperfectgem");

		if (PlayerPrefs.GetInt ("Get_SumPerfect_5000") == 0) {
			sumPerfect_v1.SetActive (true);
			sumPerfect_v2.SetActive (false);
			int get_500 = PlayerPrefs.GetInt ("Get_SumPerfect_500");
			int get_2000 = PlayerPrefs.GetInt ("Get_SumPerfect_2000");
			int get_5000 = PlayerPrefs.GetInt ("Get_SumPerfect_5000");

			for (int i = 0; i < targetNumTap_Text.Length; i++) {
				targetSumPerfect_Text [i].color = new Color (targetSumPerfect_Text [i].color.r, targetSumPerfect_Text [i].color.g, targetSumPerfect_Text [i].color.b, .5f);				
			}

			btnGet_SumPerfect.SetActive (false);
			if (s >= 500 && get_500 == 0) {
				btnGet_SumPerfect.SetActive (true);
				targetSumPerfect_Text [0].color =  new Color (targetSumPerfect_Text [0].color.r, targetSumPerfect_Text [0].color.g, targetSumPerfect_Text [0].color.b, 1);	
			} 
			if (s >= 2000 && get_2000 == 0) {
				btnGet_SumPerfect.SetActive (true);
				targetSumPerfect_Text [1].color = new Color (targetSumPerfect_Text [1].color.r, targetSumPerfect_Text [1].color.g, targetSumPerfect_Text [1].color.b, 1);	
			} 
			if (s >= 5000 && get_5000 == 0) {
				btnGet_SumPerfect.SetActive (true);
				targetSumPerfect_Text [2].color = new Color (targetSumPerfect_Text [2].color.r, targetSumPerfect_Text [2].color.g, targetSumPerfect_Text [2].color.b, 1);	
			}


			if (s <= 500) {
				bar_fill_SumPerfect.fillAmount = (float)s / 500 * 0.333f;
			} else if (s <= 2000) {
				bar_fill_SumPerfect.fillAmount = 0.333f + ((float)((s - 500)) / (2000 - 500)) * 0.333f;

			} else {
				bar_fill_SumPerfect.fillAmount = 0.666f + ((float)((s - 2000)) / (5000 - 2000)) * 0.333f;
			}
		} else {
			sumPerfect_v1.SetActive (false);
			sumPerfect_v2.SetActive (true);

			int get_10k = PlayerPrefs.GetInt ("Get_SumPerfect_10k");
			int get_15k = PlayerPrefs.GetInt ("Get_SumPerfect_15k");
			int get_20k = PlayerPrefs.GetInt ("Get_SumPerfect_20k");

			for (int i = 0; i < targetNumTap_Text2.Length; i++) {
				targetSumPerfect_Text2 [i].color = new Color (targetSumPerfect_Text2 [i].color.r, targetSumPerfect_Text2 [i].color.g, targetSumPerfect_Text2 [i].color.b, .5f);			
			}

			btnGet_SumPerfect.SetActive (false);

			if (s >= 10000 && get_10k == 0) {
				btnGet_SumPerfect.SetActive (true);
				targetSumPerfect_Text2 [0].color = new Color (targetSumPerfect_Text2 [0].color.r, targetSumPerfect_Text2 [0].color.g, targetSumPerfect_Text2 [0].color.b, 1);	
			} 
			if (s >= 15000 && get_15k == 0) {
				btnGet_SumPerfect.SetActive (true);
				targetSumPerfect_Text2 [1].color = new Color (targetSumPerfect_Text2 [1].color.r, targetSumPerfect_Text2 [1].color.g, targetSumPerfect_Text2 [1].color.b, 1);	
			} 
			if (s >= 20000 && get_20k == 0) {
				btnGet_SumPerfect.SetActive (true);
				targetSumPerfect_Text2 [2].color = new Color (targetSumPerfect_Text2 [2].color.r, targetSumPerfect_Text2 [2].color.g, targetSumPerfect_Text2 [2].color.b, 1);	
			} 

			if (s <= 10000) {
				bar_fill_SumPerfect2.fillAmount = (float)(s-5000) / (10000-5000) * 0.333f;
			} else if (s <= 15000) {
				bar_fill_SumPerfect2.fillAmount = 0.333f + ((float)((s - 1000)) / (15000 - 1000)) * 0.333f;

			} else {
				bar_fill_SumPerfect2.fillAmount = 0.666f + ((float)((s - 15000)) / (20000 - 15000)) * 0.333f;
			}
		}
	}

	void ComboX100(){
		combox100_fill.fillAmount = Combo_x_100 ();
		combox100_text.text = Combo_x_100 () + "/1";
		if (Combo_x_100 () == 0) {
			btnGet_combox100.SetActive (false);
		} else {
			btnGet_combox100.SetActive (true);

		}
	}

	void Collect3Star(){
		collect3star_fill.fillAmount = (float)Get_Collect_3_Star ()/3;
		collect3star_text.text = Get_Collect_3_Star () + "/3";
		if (Get_Collect_3_Star () < 3 ) {
			btnGet_collect3star.SetActive (false);
		} else {
			btnGet_collect3star.SetActive (true);

		}
	}


	void Update(){
		if (Input.GetMouseButtonDown (1)) {
			Set_Number_Of_Tap (5000);
			Set_Sum_Of_Perfect (5000);
			Set_Combo_x_100 (1);
			Set_Collect_3_Star (1);
			Manager.instance.PlusGem (5000);
			int l = 0;
				l++;
				PlayerPrefs.SetInt ("level", l);

		}
	}
}
