﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemPooler : MonoBehaviour {

	public static GemPooler instance;

	public GameObject prefabs;
	public List<GameObject> listObject = new List<GameObject>();


	// Use this for initialization
	void Awake () {
		instance = this;
	}

	// Update is called once per frame
	void Start () {
		SpawnObject ();
	}

	void SpawnObject(){
		for(int i = 0 ; i < 100 ; i++){
			GameObject objectClone = Instantiate (prefabs) as GameObject;
			objectClone.transform.SetParent (transform);
			objectClone.SetActive (false);
			listObject.Add (objectClone);
		}
	}

	public GameObject GetObject(){
		for (int i = 0; i < listObject.Count; i++) {
			if (listObject [i].activeInHierarchy == false) {
				listObject [i].SetActive (true);
				return listObject [i];
			}
		}
		GameObject objectClone = Instantiate (prefabs) as GameObject;
		objectClone.transform.SetParent (transform);
		objectClone.SetActive (true);
		listObject.Add (objectClone);

		return objectClone;

	}

	public void Refresh(){
		for (int i = 0; i < listObject.Count; i++) {
			listObject [i].SetActive (false);
			listObject [i].transform.localScale = Vector2.zero;
		}
	}
}
