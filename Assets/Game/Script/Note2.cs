﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Note2 : MonoBehaviour {

	public float currentSample;
	public float startSample;
	public float deltaT;
	public float t;
	Vector3 pos0,pos1,pos2,pos3;
	Vector3 current,target;
	public bool isCol;
	int mylane;
	int materColor;
	public Color[] materialsColor;
	public Material[] materials;
	MeshRenderer mesh;
	Rigidbody rig;
	Renderer rend;

	Vector3 currentScale;


	float rotateSpeed;
	bool isHit;
	bool isMiss;
	bool isPos12;
	public bool isFall;
	public LayerMask layer;

	float distance;
	public GameObject trail;
	bool isTrail;
	void Awake(){
		currentScale = transform.localScale;
	}

	public 	void OnNote(float c,float s,int lane){
		trail.SetActive (false);
		isTrail = false;
		mylane = lane;
		isMiss = false;
		isFall = false;
		isHit = false;
		isPos12 = false;
		rotateSpeed = Random.Range (10, 50) ;

		if (rig == null) {
			rig = GetComponent<Rigidbody> ();
			rend = GetComponent<Renderer> ();
			mesh = GetComponent<MeshRenderer> ();
		}
		materColor = Random.Range (0, materials.Length);
		mesh.material = materials [materColor];

		rig.velocity = Vector3.zero;
		rig.useGravity = false;
		isCol = false;
		rig.mass = 1f;

		currentSample = c;
		startSample = s;

		transform.position = new Vector3 (-500, -500, -500);

		pos0 = new Vector3 ((float)Random.Range (-50, 50)/10, (float)Random.Range (-50, 50)/10, 10);
		pos1 = new Vector3( Manager.instance.lanes [lane].transform.position.x,7,0);
		pos2 = new Vector3( pos1.x,Manager.instance.line2.transform.position.y,0);
		pos3 = new Vector3 (pos1.x, Manager.instance.frame.transform.position.y,0);
//		Debug.Log (pos0 + " ___ " + pos1 + " ___ "+ pos2 + " ___ " + pos3);
		gameObject.SetActive (true);
	}

	public void UpdateStep(){
		transform.Rotate (rotateSpeed * Time.deltaTime, rotateSpeed * Time.deltaTime, rotateSpeed * Time.deltaTime);

		if (isMiss == false && isFall == false) {

			deltaT = (Koreo.instance.sample - currentSample) / (startSample - currentSample);
			if (deltaT <= 1) {
				if (deltaT < .5f) {
					t = deltaT / 0.5f;
					current = pos0;
					target = pos1;
					float c = Mathf.Lerp (0, .5f, t);
					rend.material.color = new Color (rend.material.color.r, rend.material.color.g, rend.material.color.b, c);
					transform.localScale = Vector3.Lerp (currentScale, currentScale * 1.5f, t);

				} else if (deltaT < .85f) {
					isPos12 = true;
					t = (deltaT - .5f) / 0.35f;
					current = pos1;
					target = pos2;
					float c = Mathf.Lerp (.5f, 1, t);
					rend.material.color = new Color (rend.material.color.r, rend.material.color.g, rend.material.color.b, c);
					transform.localScale = Vector3.Lerp (currentScale * 1.5f, currentScale * 1.2f, t);
				} else {
					t = (deltaT - .85f) / .15f;
					current = pos2;
					target = pos3;
					transform.localScale = Vector3.Lerp (currentScale * 1.2f, currentScale, t);
					if (isTrail == false) {
						isTrail = true;
						trail.SetActive (true);
					}
				}

				transform.position = Vector3.Lerp (current, target, t);
				distance = 0.6f;
			} else {
				if (isFall == false) {
					isFall = true;
				}
			}
	
		}
		if (isFall && isHit == false) {
			transform.position -= new Vector3 (0, 20 * Time.deltaTime, 0);
		}
		RaycastDetect ();
	}
	RaycastHit hit;

	public void RaycastDetect(){
		if (isCol)
			return;

		if (Physics.Raycast (transform.position,Vector3.down,out hit, distance, layer)) {

			if (hit.collider == null)
				return;
			if (isCol == false) {
				if (hit.collider.gameObject.tag == "Perfect") {
					rig.useGravity = true;
					rig.mass = 1.5f;
					int r = Random.Range (0, 2);
					int f;
					if (r == 0) {
						f = Random.Range (400, 600);
					} else {
						f = Random.Range (-400, -600);
					}
					rig.AddForce (new Vector3 (f, Random.Range (400, 600)));
					isCol = true;
					isHit = true;
					EffectHit ("Perfect");
					Manager.instance.Rate ("Perfect");
					Recycle (2f);
					trail.SetActive (false);
//					rend.material.color = new Color (rend.material.color.r, rend.material.color.g, rend.material.color.b, 0.2f);


				} else if (hit.collider.gameObject.tag == "Good") {
					rig.useGravity = true;
					rig.mass = 1.5f;
					int r = Random.Range (0, 2);
					int f;
					if (r == 0) {
						f = Random.Range (400, 600);
					} else {
						f = Random.Range (-400, -600);
					}
					rig.AddForce (new Vector3 (f, Random.Range (400, 600)));
					isCol = true;
					isHit = true;

					EffectHit ("Good");
					Manager.instance.Rate ("Good");
					Recycle (2f);
					trail.SetActive (false);
//					rend.material.color = new Color (rend.material.color.r, rend.material.color.g, rend.material.color.b, .2f);

				}
			}

			if (hit.collider.gameObject.tag == "Miss") {
				isCol = true;
				isFall = true;
				Recycle (.5f);
				if(isMiss == false)
					Manager.instance.Rate ("Miss");
			}
		}
	}
		

	public void  Recycle(float time){
		StartCoroutine (recycle (time));
	}

	IEnumerator recycle(float time){

		yield return new WaitForSeconds (time);
		gameObject.SetActive (false);

	}

	GameObject particleClone;
	public void EffectHit(string type){
		if (type == "Perfect") {
//			Debug.Log (" effect perfect");
			particleClone = ParticalPooler.instance.GetObjectPerfect ();

		} else if (type == "Good") {
//			Debug.Log (" effect Good");
			particleClone = ParticalPooler.instance.GetObjectGood ();

		} 
		particleClone.transform.position = Manager.instance.frame.transform.position ;
		var main = particleClone.GetComponent<ParticleSystem> ().main;
		var mainchild = particleClone.transform.GetChild (0).GetComponent<ParticleSystem> ().main;
		main.startColor = new ParticleSystem.MinMaxGradient (materialsColor[materColor]);
		mainchild.startColor = new ParticleSystem.MinMaxGradient (materialsColor[materColor]);


	}

	public void MissNote(){
		isCol = true;
		isMiss = true;

		StartCoroutine(OnComplete());
	}

	IEnumerator OnComplete(){

		float r = (float)Random.Range (10, 100) / 100.0f;
		yield return new WaitForSeconds (r);
		rig.useGravity = true;
		rig.mass = 1.5f;
		rig.AddForce (new Vector3 (0, Random.Range(-500,-1000), 0));
		iTween.ScaleTo (gameObject, currentScale, .3f);

		yield return new WaitForSeconds (2);
		gameObject.SetActive (false);
	}
}
